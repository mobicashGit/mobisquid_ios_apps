//
//  UFXHomeUV.swift
//  PassengerApp
//
//  Created by ADMIN on 14/07/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit

class UFXHomeUV: UIViewController, OnLocationUpdateDelegate, AddressFoundDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource, MyLabelClickDelegate {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var rduView: UIView!
    @IBOutlet weak var imgSlideShow: ImageSlideshow!
    @IBOutlet weak var imgSlideShowHeight: NSLayoutConstraint!
    @IBOutlet weak var selectServiceLbl: MyLabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    //    @IBOutlet weak var bottomSpaceConstraint: NSLayoutConstraint!
    //    @IBOutlet weak var topSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var noServiceAvailLbl: MyLabel!
    @IBOutlet weak var ufxDataVerticalStackView: UIStackView!
    @IBOutlet weak var ufxSubDataStackView: UIStackView!
    
    @IBOutlet weak var rduTopBarHeaderView: UIView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var profileLbl: MyLabel!
    @IBOutlet weak var walletView: UIView!
    @IBOutlet weak var walletImgView: UIImageView!
    @IBOutlet weak var walletLbl: MyLabel!
    @IBOutlet weak var historyView: UIView!
    @IBOutlet weak var historyImgView: UIImageView!
    @IBOutlet weak var historyLbl: MyLabel!
    @IBOutlet weak var activeJobView: UIView!
    @IBOutlet weak var activeJobImgView: UIImageView!
    @IBOutlet weak var activeJobLbl: MyLabel!
    
    @IBOutlet weak var rduScrollView: UIScrollView!
    @IBOutlet weak var rduContentView: UIView!
    @IBOutlet weak var rduCollectionView: UICollectionView!
    @IBOutlet weak var rduTableView: UITableView!
    @IBOutlet weak var rduCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var rduContentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var rduSeperatorView: UIView!
    
    var getAddressFrmLocation:GetAddressFromLocation!
    
    var bannersItemList = [SDWebImageSource]()
    var cntView:UIView!
    
    var navItem:UINavigationItem!
    let generalFunc = GeneralFunctions()
    
    var loaderView:UIView!
    
    var parentCategoryItems = [NSDictionary]()
    var subCategoryItems = [NSDictionary]()
    
    var currentItems = [NSDictionary]()
    
    var currentMode = "PARENT_MODE"
    
    var selectedLatitude = ""
    var selectedLongitude = ""
    var selectedAddress = ""
    
    var userProfileJson:NSDictionary!
    
    var getLocation:GetLocation!
    
    var enterLocLbl:MyLabel!
    var locationDialog:OpenEnableLocationView!
    
    var currentLocation:CLLocation!
    var menuImage = UIImageView()
    
    var isSafeAreaSet = false
    var configPubNub:ConfigPubNub!
    
    var isFronMainScreen = false
    var defaultLocation:CLLocation!
    
    var isScreenKilled = false
    
    var isLoadParentMenu = false
    
    var listOfParentGridCategories = [NSDictionary]()
    var listOfParentListCategories = [NSDictionary]()
    var listOfParentAllCategories = [NSDictionary]()
    
    var bottomSheetCnt:BottomsheetController!
    var btnSheetCollectionView:UICollectionView!
    
    var cancelBtnSheetLbl:MyLabel!
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.configureRTLView()
        GeneralFunctions.postNotificationSignal(key: ConfigPubNub.resumeInst_key, obj: self)
        
        
//        self.setCustomNavBarColor(isCustom: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.appInBackground), name: NSNotification.Name(rawValue: Utils.appBGNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.appInForground), name: NSNotification.Name(rawValue: Utils.appFGNotificationKey), object: nil)
        self.imgSlideShow.frame.origin.y = 0
        checkLocationEnabled()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Utils.appBGNotificationKey), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Utils.appFGNotificationKey), object: nil)
        
//        self.setCustomNavBarColor(isCustom: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(navItem == nil){
            self.navItem = self.navigationItem
            isFronMainScreen = true
        }
        userProfileJson = (GeneralFunctions.getValue(key: Utils.USER_PROFILE_DICT_KEY) as! String).getJsonDataDict().getObj(Utils.message_str)

        cntView = self.generalFunc.loadView(nibName: "UFXHomeScreenDesign", uv: self, contentView: contentView) //, isStatusBarAvail: true
        //        cntView.frame.size = CGSize(width: cntView.frame.width, height: cntView.frame.height - 100)
        self.contentView.addSubview(cntView)
        
        imgSlideShow.slideshowInterval = 5.0
        
        if(Configurations.isIponeXDevice()){
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: GeneralFunctions.getSafeAreaInsets().bottom - 20, right: 0)
        }
        
        scrollView.parallaxHeader.view = self.containerView
        scrollView.parallaxHeader.height = 250
        scrollView.parallaxHeader.mode = .bottom
        scrollView.parallaxHeader.minimumHeight = 50
        scrollView.bounces = false
        
        
        loadBanners()
        
        addTitleView()
        
        setData()
        
        addMenu()
        loadServiceCategory(parentCategoryId: userProfileJson.get("UBERX_PARENT_CAT_ID"))
        
        
        if(defaultLocation != nil){
            self.onLocationUpdate(location: self.defaultLocation)
        }else{
            getLocation = GetLocation(uv: self, isContinuous: false)
            getLocation.buildLocManager(locationUpdateDelegate: self)
        }
        
        
//        GeneralFunctions.saveValue(key: "OPEN_RATING_SCREEN", value: "2229" as AnyObject)
        if(GeneralFunctions.getValue(key: "OPEN_RATING_SCREEN") != nil && (GeneralFunctions.getValue(key: "OPEN_RATING_SCREEN") as! String) != ""){
            
            let ratingUV = GeneralFunctions.instantiateViewController(pageName: "RatingUV") as! RatingUV
            ratingUV.iTripId = (GeneralFunctions.getValue(key: "OPEN_RATING_SCREEN") as! String)
            
            GeneralFunctions.removeValue(key: "OPEN_RATING_SCREEN")
            self.pushToNavController(uv:ratingUV, isDirect: true)
        }
        if(self.userProfileJson.get("APP_TYPE").uppercased() == Utils.cabGeneralType_Ride_Delivery_UberX.uppercased()){
//            navigationController?.navigationBar.barTintColor = UIColor.UCAColor.AppThemeColor_1
            
            GeneralFunctions.setImgTintColor(imgView: self.profileImgView, color: UIColor.white)
            GeneralFunctions.setImgTintColor(imgView: self.walletImgView, color: UIColor.white)
            GeneralFunctions.setImgTintColor(imgView: self.historyImgView, color: UIColor.white)
            GeneralFunctions.setImgTintColor(imgView: self.activeJobImgView, color: UIColor.white)
//
//            self.rduTopBarHeaderView.backgroundColor = UIColor.UCAColor.AppThemeColor_1
//
//            self.profileLbl.textColor = UIColor.UCAColor.AppThemeTxtColor_1
//            self.walletLbl.textColor = UIColor.UCAColor.AppThemeTxtColor_1
//            self.historyLbl.textColor = UIColor.UCAColor.AppThemeTxtColor_1
//            self.activeJobLbl.textColor = UIColor.UCAColor.AppThemeTxtColor_1
            
            self.profileLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_HEADER_RDU_PROFILE").uppercased()
            self.walletLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_HEADER_RDU_WALLET").uppercased()
            self.historyLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_HEADER_RDU_BOOKINGS").uppercased()
            self.activeJobLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_HEADER_RDU_ACTIVE_JOBS").uppercased()
            
            let profileTapGue = UITapGestureRecognizer()
            let walletTapGue = UITapGestureRecognizer()
            let historyTapGue = UITapGestureRecognizer()
            let activeJobTapGue = UITapGestureRecognizer()
            
            profileTapGue.addTarget(self, action: #selector(self.openProfile))
            walletTapGue.addTarget(self, action: #selector(self.openWallet))
            historyTapGue.addTarget(self, action: #selector(self.openHistory))
            activeJobTapGue.addTarget(self, action: #selector(self.openActiveJobs))
            
            self.profileView.isUserInteractionEnabled = true
            self.walletView.isUserInteractionEnabled = true
            self.historyView.isUserInteractionEnabled = true
            self.activeJobView.isUserInteractionEnabled = true
            
            self.profileView.addGestureRecognizer(profileTapGue)
            self.walletView.addGestureRecognizer(walletTapGue)
            self.historyView.addGestureRecognizer(historyTapGue)
            self.activeJobView.addGestureRecognizer(activeJobTapGue)
            
            
            self.rduCollectionView.dataSource = self
            self.rduCollectionView.delegate = self
            self.rduCollectionView.register(UINib(nibName: "UFXJekIconCVC", bundle: nil), forCellWithReuseIdentifier: "UFXJekIconCVC")
            self.rduCollectionView.bounces = false
            self.rduCollectionView.contentInset = UIEdgeInsetsMake(5, 5, 5, 5)
            
            self.rduTableView.dataSource = self
            self.rduTableView.delegate = self
            self.rduTableView.bounces = false
            
            self.rduTableView.tableFooterView = UIView()
            self.rduTableView.register(UINib(nibName: "UFXJekIconTVC", bundle: nil), forCellReuseIdentifier: "UFXJekIconTVC")
//            self.rduTableView.contentInset = UIEdgeInsetsMake(5, 0, GeneralFunctions.getSafeAreaInsets().bottom + 5, 0)
            
            self.rduScrollView.bounces = false
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        if(isSafeAreaSet == false){
            
            cntView.frame.size.height = cntView.frame.size.height + GeneralFunctions.getSafeAreaInsets().bottom
            isSafeAreaSet = true
        }
    }
    
    func setCustomNavBarColor(isCustom:Bool){
        if(self.userProfileJson != nil && self.userProfileJson.get("APP_TYPE").uppercased() == Utils.cabGeneralType_Ride_Delivery_UberX.uppercased()){
            //            Configurations.setAppColorNavBar(bgColor: UIColor.UCAColor.AppThemeColor_1, txtColor: UIColor.UCAColor.AppThemeTxtColor_1)
            
            if(isCustom && self.currentMode == "PARENT_MODE"){
                navigationController?.navigationBar.barTintColor = UIColor.UCAColor.AppThemeColor_1
            }else{
                navigationController?.navigationBar.barTintColor = UIColor.UCAColor.AppThemeColor
            }
        }
    }
    
    
    override func addBackBarBtn() {
        if(isLoadParentMenu){
            isLoadParentMenu = false
            super.addBackBarBtn()
            return
        }
        
        if(Configurations.isRTLMode()){
            self.navigationDrawerController?.isRightPanGestureEnabled = false
        }else{
            self.navigationDrawerController?.isLeftPanGestureEnabled = false
        }
        
        var backImg = UIImage(named: "ic_nav_bar_back")!
        if(Configurations.isRTLMode()){
            backImg = backImg.imageRotatedByDegrees(oldImage: backImg, deg: 180)
        }
        
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: backImg, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.loadParentMode))
        self.navItem.leftBarButtonItem = leftButton
    }
    
    override func closeCurrentScreen() {
        SDImageCache.shared().clearMemory()
        
        isScreenKilled = true
        
        if(getAddressFrmLocation != nil){
            getAddressFrmLocation.uv = nil
            getAddressFrmLocation.addressFoundDelegate = nil
            getAddressFrmLocation = nil
        }
        
        if(self.getLocation != nil){
            self.getLocation.locationUpdateDelegate = nil
            self.getLocation.uv = nil
            self.getLocation = nil
        }
        
        if(configPubNub != nil){
            configPubNub.ufxHomeScreenUv = nil
        }
        
        
        for subview in self.ufxDataVerticalStackView.subviews {
            subview.removeFromSuperview()
        }
        self.ufxDataVerticalStackView.removeFromSuperview()
        
        super.closeCurrentScreen()
    }
    
    func openProfile(){
        
        profileView.transform = CGAffineTransform(scaleX: 0.50, y: 0.50)
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.45, initialSpringVelocity: 3.0, options: .allowUserInteraction, animations: {
            self.profileView.transform = .identity
        }) { (_) in
            let manageProfileUv = GeneralFunctions.instantiateViewController(pageName: "ManageProfileUV") as! ManageProfileUV
            (self.navigationDrawerController?.rootViewController as! UINavigationController).pushViewController(manageProfileUv, animated: true)
        }
        
    }
    
    func openWallet(){
        
        walletView.transform = CGAffineTransform(scaleX: 0.50, y: 0.50)
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.45, initialSpringVelocity: 3.0, options: .allowUserInteraction, animations: {
            self.walletView.transform = .identity
        }) { (_) in
            let manageWalletUV = GeneralFunctions.instantiateViewController(pageName: "ManageWalletUV") as! ManageWalletUV
            (self.navigationDrawerController?.rootViewController as! UINavigationController).pushViewController(manageWalletUV, animated: true)
        }
        
    }
    
    func openHistory(){
        historyView.transform = CGAffineTransform(scaleX: 0.50, y: 0.50)
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.45, initialSpringVelocity: 3.0, options: .allowUserInteraction, animations: {
            self.historyView.transform = .identity
        }) { (_) in
            let rideHistoryUv = GeneralFunctions.instantiateViewController(pageName: "RideHistoryUV") as! RideHistoryUV
            let myBookingsUv = GeneralFunctions.instantiateViewController(pageName: "RideHistoryUV") as! RideHistoryUV
            rideHistoryUv.HISTORY_TYPE = "PAST"
            rideHistoryUv.pageTabBarItem.title = self.generalFunc.getLanguageLabel(origValue: "PAST", key: "LBL_PAST").uppercased()
            
            myBookingsUv.pageTabBarItem.title = self.generalFunc.getLanguageLabel(origValue: "UPCOMING", key: "LBL_UPCOMING").uppercased()
            myBookingsUv.HISTORY_TYPE = "LATER"
            
            if(self.userProfileJson.get("RIDE_LATER_BOOKING_ENABLED").uppercased() == "YES"){
                let rideHistoryTabUv = RideHistoryTabUV(viewControllers: [rideHistoryUv, myBookingsUv], selectedIndex: 0)
                (self.navigationDrawerController?.rootViewController as! UINavigationController).pushViewController(rideHistoryTabUv, animated: true)
            }else{
                rideHistoryUv.isDirectPush = true
                (self.navigationDrawerController?.rootViewController as! UINavigationController).pushViewController(rideHistoryUv, animated: true)
            }
        }
        
        
    }
    
    func openActiveJobs(){
        
        activeJobView.transform = CGAffineTransform(scaleX: 0.50, y: 0.50)
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.45, initialSpringVelocity: 3.0, options: .allowUserInteraction, animations: {
            self.activeJobView.transform = .identity
        }) { (_) in
            let myOnGoingTripsUV = GeneralFunctions.instantiateViewController(pageName: "MyOnGoingTripsUV") as! MyOnGoingTripsUV
            (self.navigationDrawerController?.rootViewController as! UINavigationController).pushViewController(myOnGoingTripsUV, animated: true)
        }
    }
    
    func addMenu(){
        if(isFronMainScreen){
            isLoadParentMenu = true
            self.addBackBarBtn()
        }else{
            let leftButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_menu_all_nav")!, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.openMenu))
            self.navItem.leftBarButtonItem = leftButton
        }
    }
    
    func openMenu(){
        if(Configurations.isRTLMode()){
            self.navigationDrawerController?.isRightPanGestureEnabled = true
            self.navigationDrawerController?.toggleRightView()
            
        }else{
            self.navigationDrawerController?.isLeftPanGestureEnabled = true
            self.navigationDrawerController?.toggleLeftView()
        }
    }
    
    
    func appInBackground(){
    }
    
    func appInForground(){
        checkLocationEnabled()
    }
    
    func addTitleView(){
        if(self.userProfileJson.get("APP_TYPE").uppercased() == Utils.cabGeneralType_Ride_Delivery_UberX.uppercased() && self.currentMode != "SUB_MODE"){
            let navHeight = self.navigationController!.navigationBar.frame.height
            let width = ((navHeight * 350) / 119)
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: ((width * 119) / 350)))
            imageView.contentMode = .scaleAspectFit
            
            let image = UIImage(named: "ic_your_logo")
            imageView.image = image
            
            self.navItem.titleView = imageView
        }else{
            let titleView = generalFunc.loadView(nibName: "UFXHomeTitleView", uv: self, isWithOutSize: true)
            titleView.frame = CGRect(x: 0, y:0, width: Application.screenSize.width, height: 50)
            //        titleView.subviews[2].transform = CGAffineTransform(rotationAngle: 90 * CGFloat(CGFloat.pi/180))
            (titleView.subviews[0] as! MyLabel).text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_LOCATION_FOR_AVAILING_TXT")
            (titleView.subviews[1] as! MyLabel).text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ENTER_LOC_HINT_TXT")
            (titleView.subviews[2] as! UIImageView).transform = CGAffineTransform(rotationAngle: 90 * CGFloat(CGFloat.pi/180))
            
            GeneralFunctions.setImgTintColor(imgView: (titleView.subviews[2] as! UIImageView), color: UIColor.UCAColor.AppThemeTxtColor)
            self.enterLocLbl = (titleView.subviews[1] as! MyLabel)
            
            self.navItem.titleView = titleView
            
            let titleViewTapGue = UITapGestureRecognizer()
            titleViewTapGue.addTarget(self, action: #selector(self.titleViewTapped))
            
            titleView.isUserInteractionEnabled = true
            titleView.addGestureRecognizer(titleViewTapGue)
            
            if(self.selectedAddress != ""){
                self.enterLocLbl.text = self.selectedAddress
            }
        }
        
    }
    
    func checkLocationEnabled(){
        if(locationDialog != nil){
            locationDialog.closeView()
            locationDialog = nil
        }
        
        if((GeneralFunctions.hasLocationEnabled() == false && self.currentLocation == nil) || InternetConnection.isConnectedToNetwork() == false)
        {
            
            locationDialog = OpenEnableLocationView(uv: self, containerView: self.cntView, menuImgView: UIImageView())
            locationDialog.currentInst = locationDialog
            locationDialog.setViewHandler(handler: { (latitude, longitude, address, isMenuOpen) in
                //                self.currentLocation = CLLocation(latitude: latitude, longitude: longitude)
                //                self.setTripLocation(selectedAddress: address, selectedLocation: CLLocation(latitude: latitude, longitude: longitude))
                
                if(isMenuOpen){
                    self.openMenu()
                }else{
                    self.locationDialog.closeView()
                    self.locationDialog = nil
                    self.onLocationUpdate(location: CLLocation(latitude: latitude, longitude: longitude))
                }
            })
            
            locationDialog.show()
            
            return
        }
    }
    
    func setData(){
        if(self.selectServiceLbl != nil){
            self.selectServiceLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SELECT_SERVICE_TXT")
        }
        
        configPubNub = ConfigPubNub()
        configPubNub!.ufxHomeScreenUv = self
        configPubNub!.buildPubNub()
        
    }
    
    func titleViewTapped(){
        openPlaceFinder()
    }
    
    func loadBanners(){
        if(self.userProfileJson.get("APP_TYPE").uppercased() == Utils.cabGeneralType_Ride_Delivery_UberX.uppercased()){
            return
        }
        
        let parameters = ["type":"getBanners","iMemberId": GeneralFunctions.getMemberd(), "UserType": Utils.appUserType, "WidthOfBanner": "\(Utils.getValueInPixel(value: Utils.getWidthOfBanner(widthOffset: 0)))", "HeightOfBanner": "\(Utils.getValueInPixel(value: Utils.getHeightOfBanner(widthOffset: 0)))"]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: false)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: false)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    let msgArr = dataDict.getArrObj(Utils.message_str)
                    
                    for i in 0..<msgArr.count{
                        let tempItem = msgArr[i] as! NSDictionary
                        self.bannersItemList += [SDWebImageSource(url: NSURL(string: tempItem.get("vImage").addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!)! as URL, placeholder: nil)]
                    }
                    
                    self.imgSlideShow.contentScaleMode = .scaleToFill
                    self.imgSlideShow.setImageInputs(self.bannersItemList)
                    
                    let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.sliderImgTapped))
                    self.imgSlideShow.addGestureRecognizer(recognizer)
                    
                }else{
                    //                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                //                self.generalFunc.setError(uv: self)
            }
            
        })
    }
    
    func sliderImgTapped(){
        //        imgSlideShow.presentFullScreenController(from: self)
    }
    
    func loadServiceCategory(parentCategoryId:String){
        scrollView.isHidden = true
        scrollView.scrollToTop()
        loaderView =  self.generalFunc.addMDloader(contentView: self.view)
        loaderView.backgroundColor = UIColor.clear
        
        let parameters = ["type":"getServiceCategories", "userId": GeneralFunctions.getMemberd(), "parentId": parentCategoryId, "WidthOfBanner": "\(Utils.getValueInPixel(value: Utils.getWidthOfBanner(widthOffset: 10)))", "HeightOfBanner": "\(Utils.getValueInPixel(value: Utils.getHeightOfBanner(widthOffset: 10)))", "WidthHeightOfGrid": "\(Utils.getValueInPixel(value: 60))"]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: false)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: false)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(self.isScreenKilled == true){
                return
            }
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    
                    let msgArr = dataDict.getArrObj(Utils.message_str)
                    
                    self.currentItems.removeAll()
                    if(parentCategoryId != "0"){
                        self.subCategoryItems.removeAll()
                    }
                    
                    var categoryItems = [NSDictionary] ()
                    
                    for i in 0..<msgArr.count{
                        categoryItems += [msgArr[i] as! NSDictionary]
                    }
                    
//                    if(self.userProfileJson.get("APP_TYPE").uppercased() == Utils.cabGeneralType_Ride_Delivery_UberX.uppercased() && ((parentCategoryId == "0" && self.userProfileJson.getObj("profileData").get("UBERX_PARENT_CAT_ID") == "0") || (parentCategoryId != "0" && self.userProfileJson.getObj("profileData").get("UBERX_PARENT_CAT_ID") != "0"))){
//                        self.currentItems.append(NSDictionary())
//                        self.currentItems.append(NSDictionary())
//                    }
                    self.currentItems.append(contentsOf: categoryItems)
                    
                    if(parentCategoryId == "0"){
                        self.parentCategoryItems.removeAll()
                        
//                        if(self.userProfileJson.get("APP_TYPE").uppercased() == Utils.cabGeneralType_Ride_Delivery_UberX.uppercased()){
//                            self.parentCategoryItems.append(NSDictionary())
//                            self.parentCategoryItems.append(NSDictionary())
//                        }
                        self.parentCategoryItems.append(contentsOf: categoryItems)
                        
                        self.setParentMode()
                        
                    }else{
//                        if(self.userProfileJson.get("APP_TYPE").uppercased() == Utils.cabGeneralType_Ride_Delivery_UberX.uppercased()){
//                            self.subCategoryItems.append(NSDictionary())
//                            self.subCategoryItems.append(NSDictionary())
//                        }
                        self.subCategoryItems.append(contentsOf: categoryItems)
                        self.setSubCategoryMode()
                    }
                    
                }else{
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
            
            
            self.loaderView.isHidden = true
            self.scrollView.isHidden = false
            
        })
    }
    
    func setSubCategoryMode(){
        SDImageCache.shared().clearMemory()
        self.currentMode = "SUB_MODE"
        self.addTitleView()
        self.noServiceAvailLbl.isHidden = true
        
        rduView.isHidden = true
        self.scrollView.isHidden = false
        
//        self.setCustomNavBarColor(isCustom: false)
        
        if(userProfileJson.getObj("profileData").get("UBERX_PARENT_CAT_ID") == "0"){
            //            self.scrollView.parallaxHeader.height = 0
            UIView.animate(withDuration: 0.5, animations: {
                //                self.scrollView.parallaxHeader.height = 0
                
                
            })
            
            self.scrollView.parallaxHeader.height = 50
            self.imgSlideShowHeight.constant = 0
            self.view.layoutIfNeeded()
            
            self.addBackBarBtn()
        }
        
        
        for subview in self.ufxDataVerticalStackView.subviews {
            subview.removeFromSuperview()
        }
        

        self.scrollView.scrollToTop()
        
        self.ufxSubDataStackView.isHidden = true
        //        self.ufxDataVerticalStackView.hidden = true
        
        if(self.currentItems.count == 0 && userProfileJson.getObj("profileData").get("UBERX_PARENT_CAT_ID") == "0"){
            self.noServiceAvailLbl.text = self.generalFunc.getLanguageLabel(origValue: "No services available in selected category.", key: "LBL_NO_SUB_SERVICE_AVAIL")
            self.noServiceAvailLbl.isHidden = false
        }else if(self.currentItems.count == 0){
            self.noServiceAvailLbl.text = self.generalFunc.getLanguageLabel(origValue: "No services available.", key: "LBL_NO_SERVICE_AVAIL")
            self.noServiceAvailLbl.isHidden = false
        }
        
        for i in 0..<self.currentItems.count{
            
            let horizontalStackView = (generalFunc.loadView(nibName: "UfxCategoryHorizontalStackViewDesign", uv: self).subviews[0]) as! UIStackView
            horizontalStackView.axis = .horizontal
            horizontalStackView.distribution  = UIStackViewDistribution.fillEqually
            horizontalStackView.spacing = 5
            
            
            let parentGridView = self.generalFunc.loadView(nibName: "UfxSubCategoryDesignItem", uv: self)
            parentGridView.tag = i
            
            //            parentGridView.frame.size = CGSize(width: self.view.frame.width, height: 100)
            
            //            (parentGridView.subviews[2] as! UIImageView).image = arrowImg!.imageRotatedByDegrees(270,flip: false)
            
            parentGridView.isUserInteractionEnabled = true
            let parentTapGue = UITapGestureRecognizer()
            parentTapGue.addTarget(self, action: #selector(self.itemTapped(sender:)))
            parentGridView.addGestureRecognizer(parentTapGue)
            
            let item = self.currentItems[i]
            
            //            GeneralFunctions.setImgTintColor(imgView: (parentGridView.subviews[0] as! UIImageView), color: UIColor(hex: 0x4B5B5C))
            //            GeneralFunctions.setImgTintColor(imgView: (parentGridView.subviews[2] as! UIImageView), color: UIColor(hex: 0x4B5B5C))
            
            (parentGridView.subviews[1] as! UILabel).text = item.get("vCategory")
            
            
            (parentGridView.subviews[0] as! UIImageView).sd_setImage(with: URL(string: item.get("vLogo_image")), placeholderImage: UIImage(named:"ic_no_icon"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                
            })
            
            
            (parentGridView.subviews[1] as! UILabel).textColor = UIColor(hex: 0x605F5F)
            if(Configurations.isRTLMode()){
                (parentGridView.subviews[2] as! UIImageView).transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            GeneralFunctions.setImgTintColor(imgView: (parentGridView.subviews[2] as! UIImageView), color: UIColor(hex: 0x4B5B5C))
            
            horizontalStackView.addArrangedSubview(parentGridView)
            
            if(i == (self.currentItems.count - 1)){
                (parentGridView.subviews[3]).isHidden = true
            }
            
            self.ufxDataVerticalStackView.addArrangedSubview(horizontalStackView)
        }
        self.scrollView.scrollToTop()
        
        self.ufxDataVerticalStackView.frame.size = CGSize(width: self.view.frame.width, height: CGFloat(self.currentItems.count * 105))
        self.scrollView.contentSize = CGSize(width: self.scrollView.contentSize.width, height: CGFloat(self.currentItems.count * 105) + 200)
        self.scrollView.setContentViewSize(offset: 15)
        
    }
    
    func setParentMode(){
        SDImageCache.shared().clearMemory()
        self.currentMode = "PARENT_MODE"
        self.addTitleView()
        self.noServiceAvailLbl.isHidden = true
//        self.setCustomNavBarColor(isCustom: true)
        
        
        self.ufxSubDataStackView.isHidden = true
        self.ufxDataVerticalStackView.isHidden = false
        
        self.selectServiceLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SELECT_SERVICE_TXT")
        
        if(self.userProfileJson.get("UBERX_PARENT_CAT_ID") == "0"){
            
            //            UIView.animate(withDuration: 0.5, animations: {
            //
            //            })
            self.imgSlideShowHeight.constant = Utils.getHeightOfBanner(widthOffset: 0)
            self.scrollView.parallaxHeader.height = self.imgSlideShowHeight.constant + 50
            
            self.view.layoutIfNeeded()
            
            self.containerView.frame.size = CGSize(width: self.view.frame.width, height: self.imgSlideShowHeight.constant + 50)
            
            self.addMenu()
        }
        
        for subview in self.ufxDataVerticalStackView.subviews {
            subview.removeFromSuperview()
        }
        
        
        self.scrollView.scrollToTop()
        
        if(self.currentItems.count == 0 ){
            self.noServiceAvailLbl.text = self.generalFunc.getLanguageLabel(origValue: "No services available.", key: "LBL_NO_SERVICE_AVAIL")
            self.noServiceAvailLbl.isHidden = false
        }
        
        if(self.userProfileJson.get("APP_TYPE").uppercased() == Utils.cabGeneralType_Ride_Delivery_UberX.uppercased()){
            self.noServiceAvailLbl.isHidden = true
            rduView.isHidden = false
            self.scrollView.isHidden = true
            
            
            let GRID_TILES_MAX_COUNT = GeneralFunctions.parseInt(origValue: 1, data: self.userProfileJson.get("GRID_TILES_MAX_COUNT"))
            
            listOfParentGridCategories.removeAll()
            listOfParentListCategories.removeAll()
            listOfParentAllCategories.removeAll()
            
            let rideCategory = NSMutableDictionary()
            rideCategory["eCatType"] = "Ride"
            rideCategory["vCategory"] = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_HEADER_RDU_RIDE")
            rideCategory["vCategoryBanner"] = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_HEADER_RDU_ON_DEMAND_RIDE")
            rideCategory["vLogo_image"] = self.userProfileJson.get("RIDE_GRID_ICON_NAME")
            rideCategory["vBannerImage"] = self.userProfileJson.get("RIDE_BANNER_IMG_NAME")
            
            let motoRideCategory = NSMutableDictionary()
            motoRideCategory["eCatType"] = "MotoRide"
            motoRideCategory["vCategory"] = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_HEADER_RDU_MOTO_RIDE")
            motoRideCategory["vCategoryBanner"] = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_HEADER_RDU_ON_DEMAND_MOTO_RIDE")
            motoRideCategory["vLogo_image"] = self.userProfileJson.get("MOTO_RIDE_GRID_ICON_NAME")
            motoRideCategory["vBannerImage"] = self.userProfileJson.get("MOTO_RIDE_BANNER_IMG_NAME")
            
            let deliveryCategory = NSMutableDictionary()
            deliveryCategory["eCatType"] = "Delivery"
            deliveryCategory["vCategory"] = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_HEADER_RDU_DELIVERY")
            deliveryCategory["vCategoryBanner"] = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_HEADER_RDU_ON_DEMAND_DELIVERY")
            deliveryCategory["vLogo_image"] = self.userProfileJson.get("DELIVERY_GRID_ICON_NAME")
            deliveryCategory["vBannerImage"] = self.userProfileJson.get("DELIVERY_BANNER_IMG_NAME")
            
            let motoDeliveryCategory = NSMutableDictionary()
            motoDeliveryCategory["eCatType"] = "MotoDelivery"
            motoDeliveryCategory["vCategory"] = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_HEADER_RDU_MOTO_DELIVERY")
            motoDeliveryCategory["vCategoryBanner"] = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_HEADER_RDU_ON_DEMAND_MOTO_DELIVERY")
            motoDeliveryCategory["vLogo_image"] = self.userProfileJson.get("MOTO_DELIVERY_GRID_ICON_NAME")
            motoDeliveryCategory["vBannerImage"] = self.userProfileJson.get("MOTO_DELIVERY_BANNER_IMG_NAME")
            
            
            if(self.userProfileJson.get("RIDE_SHOW_SELECTION").uppercased() == "ICON"){
                listOfParentGridCategories.append(rideCategory)
            }else if(self.userProfileJson.get("RIDE_SHOW_SELECTION").uppercased() == "ICON-BANNER"){
                listOfParentGridCategories.append(rideCategory)
                listOfParentListCategories.append(rideCategory)
            }else if(self.userProfileJson.get("RIDE_SHOW_SELECTION").uppercased() == "BANNER"){
                listOfParentListCategories.append(rideCategory)
            }
            
            if(self.userProfileJson.get("RIDE_SHOW_SELECTION").uppercased() != "NONE"){
                listOfParentAllCategories.append(rideCategory)
            }
            
            if(self.userProfileJson.get("MOTO_RIDE_SHOW_SELECTION").uppercased() == "ICON"){
                listOfParentGridCategories.append(motoRideCategory)
            }else if(self.userProfileJson.get("MOTO_RIDE_SHOW_SELECTION").uppercased() == "ICON-BANNER"){
                listOfParentGridCategories.append(motoRideCategory)
                listOfParentListCategories.append(motoRideCategory)
            }else if(self.userProfileJson.get("MOTO_RIDE_SHOW_SELECTION").uppercased() == "BANNER"){
                listOfParentListCategories.append(motoRideCategory)
            }
            
            if(self.userProfileJson.get("MOTO_RIDE_SHOW_SELECTION").uppercased() != "NONE"){
                listOfParentAllCategories.append(motoRideCategory)
            }
            
            if(self.userProfileJson.get("DELIVERY_SHOW_SELECTION").uppercased() == "ICON"){
                listOfParentGridCategories.append(deliveryCategory)
            }else if(self.userProfileJson.get("DELIVERY_SHOW_SELECTION").uppercased() == "ICON-BANNER"){
                listOfParentGridCategories.append(deliveryCategory)
                listOfParentListCategories.append(deliveryCategory)
            }else if(self.userProfileJson.get("DELIVERY_SHOW_SELECTION").uppercased() == "BANNER"){
                listOfParentListCategories.append(deliveryCategory)
            }
            
            if(self.userProfileJson.get("DELIVERY_SHOW_SELECTION").uppercased() != "NONE"){
                listOfParentAllCategories.append(deliveryCategory)
            }
            
            if(self.userProfileJson.get("MOTO_DELIVERY_SHOW_SELECTION").uppercased() == "ICON"){
                listOfParentGridCategories.append(motoDeliveryCategory)
            }else if(self.userProfileJson.get("MOTO_DELIVERY_SHOW_SELECTION").uppercased() == "ICON-BANNER"){
                listOfParentGridCategories.append(motoDeliveryCategory)
                listOfParentListCategories.append(motoDeliveryCategory)
            }else if(self.userProfileJson.get("MOTO_DELIVERY_SHOW_SELECTION").uppercased() == "BANNER"){
                listOfParentListCategories.append(motoDeliveryCategory)
            }
            
            if(self.userProfileJson.get("MOTO_DELIVERY_SHOW_SELECTION").uppercased() != "NONE"){
                listOfParentAllCategories.append(motoDeliveryCategory)
            }
            
            var gridCount = 0
            var moreAvailable = false
            var nextGridIconsItems = [NSDictionary]()
            for i in 0..<self.currentItems.count{
                let item = self.currentItems[i]

                if(item.get("eShowType").uppercased() == "ICON"){
                    if(gridCount < GRID_TILES_MAX_COUNT){
                        //                    listOfParentCategories
                        
                        listOfParentGridCategories.append(item)
                        
                    }else{
                        nextGridIconsItems.append(item)
                        moreAvailable = true
                    }
                    gridCount = gridCount + 1
                }else{
                    listOfParentListCategories.append(item)
                }
                listOfParentAllCategories.append(item)
            }
            
            
            if(moreAvailable){
                let moreCategory = NSMutableDictionary()
                moreCategory["eCatType"] = "More"
                moreCategory["vLogo_image"] = "ic_more"
                moreCategory["vCategory"] = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_MORE")
                listOfParentGridCategories.append(moreCategory)
            }
            
            
            let height = Utils.getHeightOfBanner(widthOffset: 10) + 10

            var totalGridItems = gridCount > GRID_TILES_MAX_COUNT ? GRID_TILES_MAX_COUNT : gridCount
            
            if(self.userProfileJson.get("RIDE_SHOW_SELECTION").uppercased() != "NONE" && self.userProfileJson.get("RIDE_SHOW_SELECTION").uppercased() != "BANNER"){
                totalGridItems = totalGridItems + 1
            }
            
            if(self.userProfileJson.get("MOTO_RIDE_SHOW_SELECTION").uppercased() != "NONE" && self.userProfileJson.get("MOTO_RIDE_SHOW_SELECTION").uppercased() != "BANNER"){
                totalGridItems = totalGridItems + 1
            }
            
            if(self.userProfileJson.get("DELIVERY_SHOW_SELECTION").uppercased() != "NONE" && self.userProfileJson.get("DELIVERY_SHOW_SELECTION").uppercased() != "BANNER"){
                totalGridItems = totalGridItems + 1
            }
            
            if(self.userProfileJson.get("MOTO_DELIVERY_SHOW_SELECTION").uppercased() != "NONE" && self.userProfileJson.get("MOTO_DELIVERY_SHOW_SELECTION").uppercased() != "BANNER"){
                totalGridItems = totalGridItems + 1
            }
            
            if(gridCount > GRID_TILES_MAX_COUNT || moreAvailable){
                totalGridItems = totalGridItems + 1
            }
            let totalItemInRow = Int(self.view.frame.width / 75) < 3 ? 3 : Int(self.view.frame.width / 75)
            
            var numOfRow:Double = Double(totalGridItems)/Double(totalItemInRow) < 1 ? Double(1.0) : Double(totalGridItems)/Double(totalItemInRow)
            numOfRow = numOfRow.rounded() >= numOfRow ? numOfRow.rounded() : (numOfRow.rounded() + 1.0)
            
            if(numOfRow.truncatingRemainder(dividingBy: Double(totalGridItems)) != 0){
                var numberOfRemainsItemsInRow = (totalItemInRow * Int(numOfRow)) - totalGridItems
                let numberOfRemainsItemsInRowOrig = (totalItemInRow * Int(numOfRow)) - totalGridItems
                
                var itemOfMore:NSDictionary!
                if(moreAvailable){
                    itemOfMore = listOfParentGridCategories[listOfParentGridCategories.count - 1]
                    
                    listOfParentGridCategories.remove(at: listOfParentGridCategories.count - 1)
                }
                
                var countOfRemains = 0
                while(numberOfRemainsItemsInRow > 0){
                    if(countOfRemains < nextGridIconsItems.count){
                        listOfParentGridCategories.append(nextGridIconsItems[countOfRemains])
                        numberOfRemainsItemsInRow = numberOfRemainsItemsInRow - 1
                        countOfRemains = countOfRemains + 1
                    }else{
                        numberOfRemainsItemsInRow = 0
                    }
                }
                
                if(itemOfMore != nil && numberOfRemainsItemsInRowOrig < nextGridIconsItems.count){
                    listOfParentGridCategories.append(itemOfMore)
                }
            }
            
            if(listOfParentListCategories.count == 0 || self.listOfParentGridCategories.count == 0){
                self.rduSeperatorView.isHidden = true
            }
            if(self.listOfParentGridCategories.count == 0){
                numOfRow = 0
            }
            
            self.rduCollectionViewHeight.constant = (CGFloat(numOfRow) * CGFloat(115))
            
            self.rduContentViewHeight.constant = (height * CGFloat(listOfParentListCategories.count)) + self.rduCollectionViewHeight.constant + (GeneralFunctions.getSafeAreaInsets().bottom > 0 ? (GeneralFunctions.getSafeAreaInsets().bottom - 10) : 10)
            self.rduScrollView.contentSize = CGSize(width: Application.screenSize.width, height: self.rduContentViewHeight.constant + GeneralFunctions.getSafeAreaInsets().bottom - 10)
            
            self.rduScrollView.scrollToTop()
            self.rduTableView.reloadData()
            self.rduCollectionView.reloadData()
            
        }else{
            rduView.isHidden = true
            self.scrollView.isHidden = false
            
            let totalItemInRow = Int(self.view.frame.width / 75) < 3 ? 3 : Int(self.view.frame.width / 75)
//            let numOfRow = Int(((Double(self.currentItems.count) / Double(totalItemInRow)) < 1.0 ? 1.0 : (Double(self.currentItems.count) / Double(totalItemInRow))).roundTo(places: 0))
            
            let numOfRow:Double = Double(self.currentItems.count)/Double(totalItemInRow) < 1 ? Double(1.0) : Double(self.currentItems.count)/Double(totalItemInRow)
            let numOfRow_int = Int(numOfRow.rounded() >= numOfRow ? numOfRow.rounded() : (numOfRow.rounded() + 1.0))
            
            var currentIndex = 0
            for i in 0 ..< numOfRow_int{
                
                let horizontalStackView = (generalFunc.loadView(nibName: "UfxCategoryHorizontalStackViewDesign", uv: self).subviews[0]) as! UIStackView
                horizontalStackView.axis = .horizontal
                horizontalStackView.distribution  = UIStackViewDistribution.fillEqually
                horizontalStackView.spacing = 5
                
                for j in 0 ..< totalItemInRow{
                    
                    if(currentIndex < self.currentItems.count){
                        
                        let parentGridView = self.generalFunc.loadView(nibName: "UFXJekIconCVC", uv: self) //UfxParentCategoryItemDesign
                        
                        parentGridView.tag = currentIndex
                        
                        let parentTapGue = UITapGestureRecognizer()
                        parentTapGue.addTarget(self, action: #selector(self.itemTapped(sender:)))
                        parentGridView.addGestureRecognizer(parentTapGue)
                        
                        horizontalStackView.addArrangedSubview(parentGridView)
                        
//                        if(j == totalItemInRow - 1 || (currentIndex == (self.currentItems.count - 1))){
//                            (parentGridView.subviews[2]).isHidden = true
//                        }
//
//                        if(i == numOfRow - 1){ // && (currentIndex != (self.currentItems.count - 1))
//                            (parentGridView.subviews[3]).isHidden = true
//                        }
                        
                        let item = self.currentItems[currentIndex]
                        (parentGridView.subviews[0].subviews[1] as! MyLabel).text = item.get("vCategory")

                        if(item.get("vCategory").contains(find: " ")){
                            (parentGridView.subviews[0].subviews[1] as! MyLabel).numberOfLines = 2
                        }else{
                            (parentGridView.subviews[0].subviews[1] as! MyLabel).numberOfLines = 1
                        }
                        
                        
                        
                        (parentGridView.subviews[0].subviews[1] as! MyLabel).textColor = UIColor(hex: 0x605F5F)
                        
                        (parentGridView.subviews[0].subviews[0].subviews[0] as! UIImageView).sd_setImage(with: URL(string: item.get("vLogo_image")), placeholderImage: UIImage(named:"ic_no_icon"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                            //                        GeneralFunctions.setImgTintColor(imgView: (parentGridView.subviews[0] as! UIImageView), color: UIColor(hex: 0x4B5B5C))
                        })
                        
                        parentGridView.subviews[0].subviews[0].layer.borderColor = UIColor(hex: 0xe4e4e4).cgColor
                        parentGridView.subviews[0].subviews[0].layer.borderWidth = 1
                        parentGridView.subviews[0].subviews[0].layer.masksToBounds = true
                        parentGridView.subviews[0].subviews[0].layer.cornerRadius = 15
                        
                    }else{
                        
                        let view = UIView()
                        view.backgroundColor = UIColor.clear
                        horizontalStackView.addArrangedSubview(view)
                        
                    }
                    currentIndex = currentIndex + 1
                    
                }
                
                self.ufxDataVerticalStackView.addArrangedSubview(horizontalStackView)
                
            }
            ufxDataVerticalStackView.spacing = 0
            self.ufxDataVerticalStackView.frame.size = CGSize(width: self.view.frame.width, height: CGFloat(numOfRow * 100))
            
            self.scrollView.contentSize = CGSize(width: self.scrollView.contentSize.width, height: CGFloat(numOfRow * 100) + 100)
            self.scrollView.setContentViewSize(offset: 15)
        }
        
    }
    
    func itemTapped(sender:UITapGestureRecognizer){
        if(sender.view!.tag >=  self.currentItems.count){
            return
        }
        if(loaderView != nil && loaderView.isHidden == false){
            return
        }
      
        if(userProfileJson.get("UBERX_PARENT_CAT_ID") == "0" && self.currentMode == "PARENT_MODE"){
            self.selectServiceLbl.text = self.currentItems[sender.view!.tag].get("vCategory")
            
            UIView.animate(withDuration: 0.1, animations: {
                sender.view!.transform = .init(scaleX: 0.85, y: 0.85)
            }) { (_) in
                
                UIView.animate(withDuration: 0.1, animations: {
                    sender.view!.transform = .identity
                }) { (_) in
                    self.loadServiceCategory(parentCategoryId: self.currentItems[sender.view!.tag].get("iVehicleCategoryId"))
                }
//                self.loadServiceCategory(parentCategoryId: self.currentItems[sender.view!.tag].get("iVehicleCategoryId"))
            }
            
        }else if(self.currentMode == "SUB_MODE" || userProfileJson.get("UBERX_PARENT_CAT_ID") != "0"){
            
            if(self.selectedLatitude == "" || self.selectedLongitude == ""){
                Utils.showSnakeBar(msg: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SET_LOCATION"), uv: self)
                return
            }
            
            checkSelectedCategory(iVehicleCategoryId: self.currentItems[sender.view!.tag].get("iVehicleCategoryId"), position: sender.view!.tag , categoryName: self.currentItems[sender.view!.tag].get("vCategory"))
            
            return
        }
    }
    
    func checkSelectedCategory(iVehicleCategoryId: String, position:Int , categoryName: String){
        
        let parameters = ["type":"getServiceCategoryTypes","userId": GeneralFunctions.getMemberd(), "iVehicleCategoryId": iVehicleCategoryId, "UserType": Utils.appUserType, "vLatitude": self.selectedLatitude, "vLongitude": self.selectedLongitude, "eCheck": "Yes"]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: false)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    let ufxSelectServiceUV = GeneralFunctions.instantiateViewController(pageName: "UFXServiceSelectUV") as! UFXServiceSelectUV
                    ufxSelectServiceUV.dataDict = self.currentItems[position]
                    ufxSelectServiceUV.selectedLatitude = self.selectedLatitude
                    ufxSelectServiceUV.selectedLongitude = self.selectedLongitude
                    ufxSelectServiceUV.selectedAddress = self.selectedAddress
                    self.pushToNavController(uv: ufxSelectServiceUV)
                }else{
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
            }else{
                self.generalFunc.setError(uv: self)
            }
        })
    }
    
    func loadParentMode(){
        self.currentItems.removeAll()
        self.currentItems.append(contentsOf: parentCategoryItems)
        
        setParentMode()
    }
    
    func openPlaceFinder(){
        let launchPlaceFinder = LaunchPlaceFinder(viewControllerUV: self)
        launchPlaceFinder.currInst = launchPlaceFinder
        
        if(currentLocation != nil){
            launchPlaceFinder.setBiasLocation(sourceLocationPlaceLatitude: currentLocation.coordinate.latitude, sourceLocationPlaceLongitude: currentLocation.coordinate.longitude)
        }
        
        launchPlaceFinder.initializeFinder { (address, latitude, longitude) in
            
            //            self.selectedLatitude = "\(latitude)"
            //            self.selectedLongitude = "\(longitude)"
            //            self.enterLocLbl.text = address + " ⌄"
            
            if(self.currentLocation != nil){
                self.selectedLatitude = "\(latitude)"
                self.selectedLongitude = "\(longitude)"
                self.selectedAddress = address
                self.enterLocLbl.text = address
                
            }else{
                self.onLocationUpdate(location: CLLocation(latitude: latitude, longitude: longitude))
            }
        }
    }
    
    
    func onLocationUpdate(location: CLLocation) {
        
        self.currentLocation = location
        
        if(getLocation != nil){
            getLocation.releaseLocationTask()
        }
        
        
        checkLocationEnabled()
        if(getAddressFrmLocation == nil){
            getAddressFrmLocation = GetAddressFromLocation(uv: self, addressFoundDelegate: self)
            getAddressFrmLocation.setLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            getAddressFrmLocation.setPickUpMode(isPickUpMode: true)
            getAddressFrmLocation.executeProcess(isOpenLoader: false, isAlertShow:false)
        }
        
    }
    
    func onAddressFound(address: String, location: CLLocation, isPickUpMode: Bool, dataResult: String) {
        self.selectedLatitude = "\(location.coordinate.latitude)"
        self.selectedLongitude = "\(location.coordinate.longitude)"
        self.selectedAddress = address
        if(self.enterLocLbl != nil){
            self.enterLocLbl.text = address
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listOfParentListCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == self.btnSheetCollectionView){
            return self.listOfParentAllCategories.count
        }else{
            return self.listOfParentGridCategories.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = Utils.getHeightOfBanner(widthOffset: 10)
        return height + 10
    }
    
    func getWidthOfItem() -> CGFloat{
        let totalItemInRow = Int(self.view.frame.width / 75) < 3 ? 3 : Int(self.view.frame.width / 75)
        return (Application.screenSize.width / CGFloat(totalItemInRow)) - 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: getWidthOfItem(), height: 110)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UFXJekIconTVC", for: indexPath) as! UFXJekIconTVC
        
        let item = self.listOfParentListCategories[indexPath.row]
        
        cell.categoryImgView.backgroundColor = UIColor(hex: 0xadadad)
        cell.categoryImgView.contentMode = .scaleAspectFit
        
        cell.categoryImgView.sd_setImage(with: URL(string: item.get("vBannerImage")), placeholderImage: UIImage(named:"ic_no_icon"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            //                        GeneralFunctions.setImgTintColor(imgView: (parentGridView.subviews[0] as! UIImageView), color: UIColor(hex: 0x4B5B5C))
            if(image != nil){
                cell.categoryImgView.image = image
            }else if(item.get("vBannerImage") != "" && !item.get("vBannerImage").contains(find: "http")){
                cell.categoryImgView.image = UIImage(named: item.get("vBannerImage"))
            }else{                
                cell.categoryImgView.image = UIImage(named: "ic_no_icon")
            }
        })
        
        cell.categoryLbl.text = item.get("vCategoryBanner") != "" ? item.get("vCategoryBanner") : item.get("vCategory")
        
        cell.bookNowLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BOOK_NOW")
        cell.bookNowLbl.backgroundColor = UIColor.UCAColor.AppThemeColor
        cell.bookNowLbl.textColor = UIColor.UCAColor.AppThemeTxtColor
        
        cell.bookNowLbl.layer.masksToBounds = true
        cell.bookNowLbl.layer.cornerRadius = 5
        
        cell.categoryLbl.layer.masksToBounds = true
        cell.categoryLbl.layer.cornerRadius = 5
        
        
        cell.categoryImgView.layer.borderColor = UIColor(hex: 0xe4e4e4).cgColor
        cell.categoryImgView.layer.borderWidth = 1
        cell.categoryImgView.layer.masksToBounds = true
        cell.categoryImgView.layer.cornerRadius = 10
        
        
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UFXJekIconCVC", for: indexPath) as! UFXJekIconCVC
        
        var item:NSDictionary!
        
        if(collectionView == self.btnSheetCollectionView){
            item = self.listOfParentAllCategories[indexPath.item]
        }else{
            item = self.listOfParentGridCategories[indexPath.item]
        }
        
        cell.iconImgView.sd_setImage(with: URL(string: item.get("vLogo_image")), placeholderImage: UIImage(named:"ic_no_icon"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            //                        GeneralFunctions.setImgTintColor(imgView: (parentGridView.subviews[0] as! UIImageView), color: UIColor(hex: 0x4B5B5C))
            if(image != nil){
                cell.iconImgView.image = image
            }else if(item.get("vLogo_image") != "" && !item.get("vLogo_image").contains(find: "http")){
                cell.iconImgView.image = UIImage(named: item.get("vLogo_image"))
            }else{
                cell.iconImgView.image = UIImage(named: "ic_no_icon")
            }
        })
        
        cell.bgView.layer.borderColor = UIColor(hex: 0xe4e4e4).cgColor
        cell.bgView.layer.borderWidth = 1
        cell.bgView.layer.masksToBounds = true
        cell.bgView.layer.cornerRadius = 15
        
        cell.categoryLbl.text = item.get("vCategory")
        
        if(item.get("vCategory").contains(find: " ")){
            cell.categoryLbl.numberOfLines = 2
        }else{
            cell.categoryLbl.numberOfLines = 1
        }
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(loaderView != nil && loaderView.isHidden == false){
            return
        }
        
        UIView.animate(withDuration: 0.1, animations: {
            if let cell = tableView.cellForRow(at: indexPath) as? UFXJekIconTVC {
                cell.transform = .init(scaleX: 0.95, y: 0.95)
            }
        }) { (_) in
            if let cell = tableView.cellForRow(at: indexPath) as? UFXJekIconTVC {
                cell.transform = .identity
            }
        }
        
        if(self.userProfileJson.get("APP_TYPE").uppercased() == Utils.cabGeneralType_Ride_Delivery_UberX.uppercased()){
//             && (self.userProfileJson.get("RIDE_DELIVERY_SHOW_TYPE").uppercased() == "BANNER" || self.userProfileJson.get("RIDE_DELIVERY_SHOW_TYPE").uppercased() == "ICON-BANNER")
            if(self.listOfParentListCategories[indexPath.row].get("eCatType").uppercased() == "RIDE"){
                let mainScreenUv = GeneralFunctions.instantiateViewController(pageName: "MainScreenUV") as! MainScreenUV
                mainScreenUv.APP_TYPE = "Ride"
                mainScreenUv.vTitleFromUFX = self.listOfParentListCategories[indexPath.row].get("vCategory")
                mainScreenUv.isFromUFXhomeScreen = true
                self.pushToNavController(uv: mainScreenUv)
                return
                
            }else if(self.listOfParentListCategories[indexPath.row].get("eCatType").uppercased() == "MOTORIDE"){
                let mainScreenUv = GeneralFunctions.instantiateViewController(pageName: "MainScreenUV") as! MainScreenUV
                mainScreenUv.APP_TYPE = "Ride"
                mainScreenUv.vTitleFromUFX = self.listOfParentListCategories[indexPath.row].get("vCategory")
                mainScreenUv.eShowOnlyMoto = true
                mainScreenUv.isFromUFXhomeScreen = true
                self.pushToNavController(uv: mainScreenUv)
                return
                
            }else if(self.listOfParentListCategories[indexPath.row].get("eCatType").uppercased() == "DELIVERY"){
                let mainScreenUv = GeneralFunctions.instantiateViewController(pageName: "MainScreenUV") as! MainScreenUV
                mainScreenUv.APP_TYPE = "Delivery"
                mainScreenUv.vTitleFromUFX = self.listOfParentListCategories[indexPath.row].get("vCategory")
                mainScreenUv.isFromUFXhomeScreen = true
                self.pushToNavController(uv: mainScreenUv)
                return
            }else if(self.listOfParentListCategories[indexPath.row].get("eCatType").uppercased() == "MOTODELIVERY"){
                let mainScreenUv = GeneralFunctions.instantiateViewController(pageName: "MainScreenUV") as! MainScreenUV
                mainScreenUv.APP_TYPE = "Delivery"
                mainScreenUv.eShowOnlyMoto = true
                mainScreenUv.vTitleFromUFX = self.listOfParentListCategories[indexPath.row].get("vCategory")
                mainScreenUv.isFromUFXhomeScreen = true
                self.pushToNavController(uv: mainScreenUv)
                return
            }
        }
        
        if(userProfileJson.get("UBERX_PARENT_CAT_ID") == "0" && self.currentMode == "PARENT_MODE"){
            self.selectServiceLbl.text = self.listOfParentListCategories[indexPath.row].get("vCategory")
            loadServiceCategory(parentCategoryId: self.listOfParentListCategories[indexPath.row].get("iVehicleCategoryId"))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(loaderView != nil && loaderView.isHidden == false){
            return
        }
        
        UIView.animate(withDuration: 0.1, animations: {
            if let cell = collectionView.cellForItem(at: indexPath) as? UFXJekIconCVC {
                cell.transform = .init(scaleX: 0.85, y: 0.85)
            }
        }) { (_) in
            if let cell = collectionView.cellForItem(at: indexPath) as? UFXJekIconCVC {
                cell.transform = .identity
            }
        }
        
        if(self.bottomSheetCnt != nil){
            
            self.bottomSheetCnt.dismissView(viewDismissHandler: { (isViewDismissed) in
                self.bottomSheetCnt = nil
                self.continueCollectionViewItemClick(collectionView: collectionView, indexPath: indexPath)
            })
            
//            self.bottomSheetCnt.dismiss(animated: true, completion: {
//                self.bottomSheetCnt = nil
//                self.continueCollectionViewItemClick(collectionView: collectionView, indexPath: indexPath)
//            })
        }else{
            continueCollectionViewItemClick(collectionView: collectionView, indexPath: indexPath)
        }
        
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.1) {
            if let cell = tableView.cellForRow(at: indexPath) as? UFXJekIconTVC {
                cell.transform = .init(scaleX: 0.95, y: 0.95)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.1) {
            if let cell = tableView.cellForRow(at: indexPath) as? UFXJekIconTVC {
                cell.transform = .identity
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.1) {
            if let cell = collectionView.cellForItem(at: indexPath) as? UFXJekIconCVC {
                cell.transform = .init(scaleX: 0.85, y: 0.85)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.1) {
            if let cell = collectionView.cellForItem(at: indexPath) as? UFXJekIconCVC {
                cell.transform = .identity
            }
        }
    }
    
    func continueCollectionViewItemClick(collectionView: UICollectionView, indexPath: IndexPath){
        
        var item:NSDictionary!
        
        if(collectionView == self.btnSheetCollectionView){
            item = self.listOfParentAllCategories[indexPath.row]
            
        }else{
            item = self.listOfParentGridCategories[indexPath.row]
        }
        
        if(self.userProfileJson.get("APP_TYPE").uppercased() == Utils.cabGeneralType_Ride_Delivery_UberX.uppercased()){
//             && (self.userProfileJson.get("RIDE_DELIVERY_SHOW_TYPE").uppercased() == "ICON" || self.userProfileJson.get("RIDE_DELIVERY_SHOW_TYPE").uppercased() == "ICON-BANNER")) || (self.userProfileJson.get("APP_TYPE").uppercased() == Utils.cabGeneralType_Ride_Delivery_UberX.uppercased() && collectionView == self.btnSheetCollectionView)
            if(item.get("eCatType").uppercased() == "RIDE"){
                let mainScreenUv = GeneralFunctions.instantiateViewController(pageName: "MainScreenUV") as! MainScreenUV
                mainScreenUv.APP_TYPE = "Ride"
                mainScreenUv.vTitleFromUFX = item.get("vCategory")
                mainScreenUv.isFromUFXhomeScreen = true
                self.pushToNavController(uv: mainScreenUv)
                return
                
            }else if(item.get("eCatType").uppercased() == "MOTORIDE"){
                let mainScreenUv = GeneralFunctions.instantiateViewController(pageName: "MainScreenUV") as! MainScreenUV
                mainScreenUv.APP_TYPE = "Ride"
                mainScreenUv.vTitleFromUFX = item.get("vCategory")
                mainScreenUv.eShowOnlyMoto = true
                mainScreenUv.isFromUFXhomeScreen = true
                self.pushToNavController(uv: mainScreenUv)
                return
                
            }else if(item.get("eCatType").uppercased() == "DELIVERY"){
                let mainScreenUv = GeneralFunctions.instantiateViewController(pageName: "MainScreenUV") as! MainScreenUV
                mainScreenUv.APP_TYPE = "Delivery"
                mainScreenUv.vTitleFromUFX = item.get("vCategory")
                mainScreenUv.isFromUFXhomeScreen = true
                self.pushToNavController(uv: mainScreenUv)
                return
            }else if(item.get("eCatType").uppercased() == "MOTODELIVERY"){
                let mainScreenUv = GeneralFunctions.instantiateViewController(pageName: "MainScreenUV") as! MainScreenUV
                mainScreenUv.APP_TYPE = "Delivery"
                mainScreenUv.vTitleFromUFX = item.get("vCategory")
                mainScreenUv.isFromUFXhomeScreen = true
                mainScreenUv.eShowOnlyMoto = true
                self.pushToNavController(uv: mainScreenUv)
                return
            }
        }
        
        let eCatType = item.get("eCatType")
        if(eCatType.uppercased() == "MORE"){
            
            bottomSheetCnt = Bottomsheet.Controller()
            
            let currentUV = self
            // Adds NavigationBar
            bottomSheetCnt.addNavigationbar { [weak self] navigationBar in
                // navigationBar
                let widthOfCancelTxt = self?.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TXT").width(withConstrainedHeight: 44, font: UIFont(name: "Roboto-Light", size: 17)!)
                
                self?.cancelBtnSheetLbl = MyLabel(frame: CGRect(x: Application.screenSize.width - 10 - widthOfCancelTxt!, y: 0, width: Application.screenSize.width, height: 44))
                self?.cancelBtnSheetLbl.text = self?.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TXT")
                self?.cancelBtnSheetLbl.textAlignment = .natural
                self?.cancelBtnSheetLbl.textColor = UIColor.UCAColor.AppThemeTxtColor
                self?.cancelBtnSheetLbl.setClickDelegate(clickDelegate: currentUV)
                self?.cancelBtnSheetLbl.font = UIFont(name: "Roboto-Light", size: 17)!
                
                let titleView = MyLabel(frame: CGRect(x: 10, y: 0, width: Application.screenSize.width, height: 44))
                titleView.text = self?.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SELECT_SERVICE")
                titleView.textAlignment = .natural
                titleView.textColor = UIColor.UCAColor.AppThemeTxtColor
                titleView.font = UIFont(name: "Roboto-Light", size: 17)!
                
                navigationBar.addSubview(titleView)
                navigationBar.addSubview((self?.cancelBtnSheetLbl)!)
            }
            
            // Adds CollectionView
            bottomSheetCnt.addCollectionView(isScrollEnabledInSheet: false) { [weak self] collectionView in
                // collectionView
                self?.btnSheetCollectionView = collectionView
                
                self?.btnSheetCollectionView.delegate = self
                self?.btnSheetCollectionView.dataSource = self
                
                collectionView.backgroundColor = .white
                self?.btnSheetCollectionView.register(UINib(nibName: "UFXJekIconCVC", bundle: nil), forCellWithReuseIdentifier: "UFXJekIconCVC")
                self?.btnSheetCollectionView.bounces = false
                self?.btnSheetCollectionView.contentInset = UIEdgeInsetsMake(Configurations.isIponeXDevice() ? GeneralFunctions.getSafeAreaInsets().top : 64, 5, 5, 5)
            }
            
            bottomSheetCnt.overlayBackgroundColor = UIColor.black.withAlphaComponent(0.6)
            bottomSheetCnt.containerViewBackgroundColor = UIColor.white
            
            bottomSheetCnt.viewActionType = .tappedDismiss
            bottomSheetCnt.initializeHeight = 350
            
            self.bottomSheetCnt.setViewDismissHandler(viewDismissHandler: { (isViewDismissed) in
                self.bottomSheetCnt = nil
            })
            
            self.present(bottomSheetCnt, animated: true, completion: nil)
            
        }else{
            if(userProfileJson.get("UBERX_PARENT_CAT_ID") == "0" && self.currentMode == "PARENT_MODE"){
                self.selectServiceLbl.text = item.get("vCategory")
                loadServiceCategory(parentCategoryId: item.get("iVehicleCategoryId"))
            }
        }
    }
    
    func myLableTapped(sender: MyLabel) {
        if(self.cancelBtnSheetLbl != nil && sender == cancelBtnSheetLbl){
            if(self.bottomSheetCnt != nil){
                self.bottomSheetCnt.dismissView(viewDismissHandler: { (isViewDismissed) in
                    self.bottomSheetCnt = nil
                })
            }
        }
    }
    
    @IBAction func unwindToUFXHomeScreen(_ segue:UIStoryboardSegue) {
        //        unwindToSignUp
        
        if(segue.source.isKind(of: MainScreenUV.self)){
            // Called when booking is successfully finished
            
            let mainScreenUv = segue.source as! MainScreenUV
            
            //            loadParentMode()
            loadServiceCategory(parentCategoryId: userProfileJson.get("UBERX_PARENT_CAT_ID"))
            
            let openBookingFinishedView = OpenBookingFinishedView(uv: self, containerView: self.contentView)
            openBookingFinishedView.currentInst = openBookingFinishedView
            openBookingFinishedView.ufxDriverAcceptedReqNow = mainScreenUv.ufxDriverAcceptedReqNow
            openBookingFinishedView.ufxReqLater = mainScreenUv.selectedDate != "" ? true : false
            openBookingFinishedView.show()
        }
    }
}

