//
//  UFXSelectPaymentModeUV.swift
//  PassengerApp
//
//  Created by ADMIN on 16/10/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit

class UFXSelectPaymentModeUV: UIViewController, MyBtnClickDelegate {

    @IBOutlet weak var selectPayModeLbl: MyLabel!
    @IBOutlet weak var contentView:UIView!
    @IBOutlet weak var cashPayModeView: UIView!
    @IBOutlet weak var cardPayModeView: UIView!
    @IBOutlet weak var cashPayLbl: MyLabel!
    @IBOutlet weak var cardPayLbl: MyLabel!
    @IBOutlet weak var continueBtn: MyButton!
    @IBOutlet weak var cashPayImgView: UIImageView!
    @IBOutlet weak var cardPayImgView: UIImageView!
    
    //Confirm Card Outlets
    @IBOutlet weak var confirmCardHLbl: MyLabel!
    @IBOutlet weak var confirmCardVLbl: MyLabel!
    @IBOutlet weak var confirmCardLbl: MyLabel!
    @IBOutlet weak var changeCardLbl: MyLabel!
    @IBOutlet weak var cancelCardLbl: MyLabel!
    
    // OutStanding Amount related outlets
    @IBOutlet weak var outStandingAmtTopView: UIView!
    @IBOutlet weak var outStandingAmtLbl: MyLabel!
    @IBOutlet weak var outStandingAmtValueLbl: MyLabel!
    @IBOutlet weak var payNowView: UIView!
    @IBOutlet weak var payNowViewHeight: NSLayoutConstraint!
    @IBOutlet weak var adjustAmtView: UIView!
    @IBOutlet weak var cancelOutAmtBtn: MyButton!
    @IBOutlet weak var payNowLbl: MyLabel!
    @IBOutlet weak var adjustAmtLbl: MyLabel!
    @IBOutlet weak var payNowImgView: UIImageView!
    @IBOutlet weak var adjustAmtImgView: UIImageView!
    
    var isOutStandingAmtSkipped = false
    
    var checkCardMode = ""
    
    let generalFunc = GeneralFunctions()
    
    var ufxConfirmServiceUV:UFXConfirmServiceUV!
    
    var userProfileJson:NSDictionary!
    
    let cashTapGue = UITapGestureRecognizer()
    let cardTapGue = UITapGestureRecognizer()
    
    var isCashPayment = true
    var isCardValidated = false
    
    var appliedPromoCode = ""
    var specialInstruction = ""
    
    var confirmCardDialogView:UIView!
    var confirmCardBGDialogView:UIView!
    
    var selectedProviderData:NSDictionary!
    
    var cntView:UIView!
    
    var isBookLater = false
    var isCashPayDisabled = false
    
    var outStandingAmtView:UIView!
    var outStandingAmtBGView:UIView!
    
    var isAutoContinue_payBox = false
    
    override func viewWillAppear(_ animated: Bool) {
        self.configureRTLView()
        
        self.userProfileJson =  (GeneralFunctions.getValue(key: Utils.USER_PROFILE_DICT_KEY) as! String).getJsonDataDict().getObj(Utils.message_str)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cntView = self.generalFunc.loadView(nibName: "UFXSelectPaymentModeScreenDesign", uv: self, contentView: contentView)
        self.contentView.addSubview(cntView)
        
        self.userProfileJson =  (GeneralFunctions.getValue(key: Utils.USER_PROFILE_DICT_KEY) as! String).getJsonDataDict().getObj(Utils.message_str)
        setData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if(isBookLater == false){
            self.continueBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "Book Now", key: "LBL_BOOK_NOW"))
        }else{
             self.continueBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "Confirm Booking", key: "LBL_CONFIRM_BOOKING"))
        }
    }
    
    func setData(){
        selectPayModeLbl.text = self.generalFunc.getLanguageLabel(origValue: "How would you like to pay?", key: "LBL_HOW_TO_PAY_TXT")
        self.cashPayLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CASH_PAYMENT_TXT")
        self.cardPayLbl.text = self.generalFunc.getLanguageLabel(origValue: "Pay online", key: "LBL_PAY_ONLINE_TXT")
        
        if(self.userProfileJson.get("APP_PAYMENT_MODE") == "Card"){
            isCashPayment = false
            self.cashPayModeView.isHidden = true
            
            
            GeneralFunctions.setImgTintColor(imgView: cashPayImgView, color: UIColor(hex: 0xd3d3d3))
            GeneralFunctions.setImgTintColor(imgView: cardPayImgView, color: UIColor.UCAColor.AppThemeColor)
            
        }else if(self.userProfileJson.get("APP_PAYMENT_MODE") == "Cash"){
            isCashPayment = true
            self.cardPayModeView.isHidden = true
            
            GeneralFunctions.setImgTintColor(imgView: cardPayImgView, color: UIColor(hex: 0xd3d3d3))
            GeneralFunctions.setImgTintColor(imgView: cashPayImgView, color: UIColor.UCAColor.AppThemeColor)
            
        }else{
            isCashPayment = true
            
            GeneralFunctions.setImgTintColor(imgView: cardPayImgView, color: UIColor(hex: 0xd3d3d3))
            GeneralFunctions.setImgTintColor(imgView: cashPayImgView, color: UIColor.UCAColor.AppThemeColor)
        }
        
        if(self.selectedProviderData != nil){
            
            if(selectedProviderData.get("ACCEPT_CASH_TRIPS") == "No" && self.cashPayModeView.isHidden == false){
                isCashPayment = false
                isCashPayDisabled = true
                
                cashPayImgView.image = UIImage(named: "ic_select_false")
                cardPayImgView.image = UIImage(named: "ic_select_true")
                
                GeneralFunctions.setImgTintColor(imgView: cashPayImgView, color: UIColor(hex: 0xd3d3d3))
                GeneralFunctions.setImgTintColor(imgView: cardPayImgView, color: UIColor.UCAColor.AppThemeColor)
            }
        }
        
        cashTapGue.addTarget(self, action: #selector(self.cashViewTapped))
        cardTapGue.addTarget(self, action: #selector(self.cardViewTapped))
        
        cashPayModeView.isUserInteractionEnabled = true
        cardPayModeView.isUserInteractionEnabled = true
        
        cashPayModeView.addGestureRecognizer(cashTapGue)
        cardPayModeView.addGestureRecognizer(cardTapGue)
        
        self.continueBtn.clickDelegate = self
    }
    
    func cashViewTapped(){
        if(isCashPayDisabled == true){
            self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "Your selected provider can't accept cash payment.", key: "LBL_CASH_DISABLE_PROVIDER"))
            return
        }
        self.isCashPayment = true
        cashPayImgView.image = UIImage(named: "ic_select_true")
        cardPayImgView.image = UIImage(named: "ic_select_false")
        
        GeneralFunctions.setImgTintColor(imgView: cardPayImgView, color: UIColor(hex: 0xd3d3d3))
        GeneralFunctions.setImgTintColor(imgView: cashPayImgView, color: UIColor.UCAColor.AppThemeColor)
    }
    
    func cardViewTapped(){
        checkCardConfig()
    }
    
    func checkCardConfig(){
        if(userProfileJson.get("vStripeCusId") != "" || userProfileJson.get("vBrainTreeCustId") != "" || userProfileJson.get("vPaymayaCustId") != "" || userProfileJson.get("vOmiseCustId") != "" || userProfileJson.get("vAdyenToken") != ""){
            showPaymentBox()
        }else{
            let paymentUV = GeneralFunctions.instantiateViewController(pageName: "PaymentUV") as! PaymentUV
            paymentUV.isFromUFXPayMode = true
            self.pushToNavController(uv: paymentUV)
        }
    }
    
    func changeCard(){
        closeConfirmCardView()
        let paymentUV = GeneralFunctions.instantiateViewController(pageName: "PaymentUV") as! PaymentUV
        paymentUV.isFromUFXPayMode = true
        self.pushToNavController(uv: paymentUV)
    }
    
    
    func showPaymentBox(){
        
        confirmCardDialogView = self.generalFunc.loadView(nibName: "UFXConfirmCardView", uv: self, isWithOutSize: true)
        
        let width = Application.screenSize.width  > 390 ? 375 : Application.screenSize.width - 50
        
        confirmCardDialogView.frame.size = CGSize(width: width, height: 200)
        
        confirmCardDialogView.center = CGPoint(x: self.contentView.bounds.midX, y: self.contentView.bounds.midY)
        
        confirmCardBGDialogView = UIView()
        confirmCardBGDialogView.backgroundColor = UIColor.black
        confirmCardBGDialogView.alpha = 0.4
        confirmCardBGDialogView.isUserInteractionEnabled = true
        
        let bgViewTapGue = UITapGestureRecognizer()
        confirmCardBGDialogView.frame = self.contentView.frame
        
        confirmCardBGDialogView.center = CGPoint(x: self.contentView.bounds.midX, y: self.contentView.bounds.midY)
        
        bgViewTapGue.addTarget(self, action: #selector(self.closeConfirmCardView))
        
        confirmCardBGDialogView.addGestureRecognizer(bgViewTapGue)
        
        confirmCardDialogView.layer.shadowOpacity = 0.5
        confirmCardDialogView.layer.shadowOffset = CGSize(width: 0, height: 3)
        confirmCardDialogView.layer.shadowColor = UIColor.black.cgColor
        
        
        self.view.addSubview(confirmCardBGDialogView)
        self.view.addSubview(confirmCardDialogView)
        
        let cancelConfirmCardTapGue = UITapGestureRecognizer()
        cancelConfirmCardTapGue.addTarget(self, action: #selector(self.closeConfirmCardView))
        
        cancelCardLbl.isUserInteractionEnabled = true
        cancelCardLbl.addGestureRecognizer(cancelConfirmCardTapGue)
        
        let confirmCardTapGue = UITapGestureRecognizer()
        confirmCardTapGue.addTarget(self, action: #selector(self.checkCard))
        
        confirmCardLbl.isUserInteractionEnabled = true
        confirmCardLbl.addGestureRecognizer(confirmCardTapGue)
        
        let changeCardTapGue = UITapGestureRecognizer()
        changeCardTapGue.addTarget(self, action: #selector(self.changeCard))
        
        changeCardLbl.isUserInteractionEnabled = true
        changeCardLbl.addGestureRecognizer(changeCardTapGue)
        
        self.confirmCardHLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_TITLE_PAYMENT_ALERT")
        
        Utils.createRoundedView(view: confirmCardDialogView, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 10)
        
        self.confirmCardLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_TRIP_CANCEL_CONFIRM_TXT")
        self.cancelCardLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TXT")
        self.changeCardLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CHANGE")
        
        self.confirmCardVLbl.text = self.userProfileJson.get("vCreditCard")
        
        if(userProfileJson.get("vBrainTreeCustEmail") != "" && userProfileJson.get("APP_PAYMENT_METHOD") == "Braintree"){
            self.confirmCardHLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PAYPAL_EMAIL_TXT")
            self.confirmCardVLbl.text = userProfileJson.get("vBrainTreeCustEmail")
        }
    }
    
    
    func closeConfirmCardView(){
        if(confirmCardBGDialogView != nil){
            confirmCardBGDialogView.removeFromSuperview()
            confirmCardBGDialogView = nil
        }
        
        if(confirmCardDialogView != nil){
            confirmCardDialogView.removeFromSuperview()
            confirmCardDialogView = nil
        }
    }
    
    func checkCard(){
        closeConfirmCardView()
        
//        let parameters = ["type":"CheckCard", "iUserId": GeneralFunctions.getMemberd()]
        
        let parameters = ["type": "\(self.checkCardMode == "OUT_STAND_AMT" ? "ChargePassengerOutstandingAmount" : "CheckCard")","\(self.checkCardMode == "OUT_STAND_AMT" ? "iMemberId" : "iUserId")": GeneralFunctions.getMemberd(), "UserType": Utils.appUserType]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    if(self.checkCardMode == "OUT_STAND_AMT"){
//                        if(self.userProfileJson.get("APP_PAYMENT_MODE").uppercased() == "CARD"){
//                            self.selectCard()
//                        }
                        
                        GeneralFunctions.saveValue(key: Utils.USER_PROFILE_DICT_KEY, value: response as AnyObject)
                        self.userProfileJson = (GeneralFunctions.getValue(key: Utils.USER_PROFILE_DICT_KEY) as! String).getJsonDataDict().getObj(Utils.message_str)
                        
                    }else{
                        self.selectCard()
                    }
//                    self.selectCard()
                    
                    if(self.isAutoContinue_payBox == true){
                        if(self.checkCardMode != "OUT_STAND_AMT"){
                            self.isOutStandingAmtSkipped = true
                            self.continueBtn.btnTapped()
                        }else{
                            
                            self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message1")), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                                self.continueBtn.btnTapped()
                            })
                        }
                        
                    }else{
                        if(self.checkCardMode == "OUT_STAND_AMT"){
                            self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message1")))
                        }
                    }
                    
                }else{
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
        })
    }
    
    func selectCard(){
        self.isCardValidated = true
        self.isCashPayment = false
        
        self.cashPayImgView.image = UIImage(named: "ic_select_false")
        self.cardPayImgView.image = UIImage(named: "ic_select_true")
        
        GeneralFunctions.setImgTintColor(imgView: cashPayImgView, color: UIColor(hex: 0xd3d3d3))
        GeneralFunctions.setImgTintColor(imgView: cardPayImgView, color: UIColor.UCAColor.AppThemeColor)
    }
    
    func openAccountVerify(){
        
        let accountVerificationUv = GeneralFunctions.instantiateViewController(pageName: "AccountVerificationUV") as! AccountVerificationUV
        if(userProfileJson.get("eEmailVerified").uppercased() != "YES" && userProfileJson.get("ePhoneVerified").uppercased() != "YES" ){
            accountVerificationUv.requestType = "DO_EMAIL_PHONE_VERIFY"
        }else if(userProfileJson.get("eEmailVerified").uppercased() != "YES"){
            accountVerificationUv.requestType = "DO_EMAIL_VERIFY"
        }else{
            accountVerificationUv.requestType = "DO_PHONE_VERIFY"
        }
        accountVerificationUv.ufxSelectPaymentModeUv = self
        self.pushToNavController(uv: accountVerificationUv)
    }
    
    func myBtnTapped(sender: MyButton) {
        if(sender == self.continueBtn){
            /**
             * Check for out standing amount. User will not be able to do trip without paying out standing amount.
             */
            if(self.userProfileJson.get("fOutStandingAmount") != "" && GeneralFunctions.parseDouble(origValue: 0.0, data: self.userProfileJson.get("fOutStandingAmount")) > 0 && isOutStandingAmtSkipped == false){
                self.openOutStandingAmountBox()
                return
            }
            
            if(self.isCardValidated == false && self.isCashPayment == false){
               self.checkCardMode = ""
                isAutoContinue_payBox = true
                checkCardConfig()
                return
            }
            
            if(userProfileJson.get("eEmailVerified").uppercased() != "YES" || userProfileJson.get("ePhoneVerified").uppercased() != "YES"){
                self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ACCOUNT_VERIFY_ALERT_RIDER_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TXT"), completionHandler: { (btnClickedIndex) in
                    
                    if(btnClickedIndex == 0){
                        self.openAccountVerify()
                    }
                    
                })
            }else{
                self.performSegue(withIdentifier: "unwindToMainScreen", sender: self)
            }
            
        }else if(self.outStandingAmtView != nil && sender == self.cancelOutAmtBtn){
            self.closeOutStandingAmtView()
        }
    }
    
    func openOutStandingAmountBox(){
        
        
        outStandingAmtView = self.generalFunc.loadView(nibName: "OutStandingAmtView", uv: self, isWithOutSize: true)
        
        let width = Application.screenSize.width  > 390 ? 375 : Application.screenSize.width - 50
        
        if(self.userProfileJson.get("APP_PAYMENT_MODE") == "Cash"){
            outStandingAmtView.frame.size = CGSize(width: width, height: 285)
        }else{
            outStandingAmtView.frame.size = CGSize(width: width, height: 335)
        }
        
        outStandingAmtView.center = CGPoint(x: self.cntView.bounds.midX, y: self.cntView.bounds.midY)
        
        outStandingAmtBGView = UIView()
        outStandingAmtBGView.frame = CGRect(x: 0, y: 0, width: Application.screenSize.width, height: Application.screenSize.height)
        outStandingAmtBGView.backgroundColor = UIColor.black
        self.outStandingAmtBGView.alpha = 0
        outStandingAmtBGView.isUserInteractionEnabled = true
        
        outStandingAmtView.layer.shadowOpacity = 0.5
        outStandingAmtView.layer.shadowOffset = CGSize(width: 0, height: 3)
        outStandingAmtView.layer.shadowColor = UIColor.black.cgColor
        
        outStandingAmtView.alpha = 0
        self.view.addSubview(outStandingAmtBGView)
        self.view.addSubview(outStandingAmtView)
        
        self.outStandingAmtLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_OUTSTANDING_AMOUNT_TXT")
        self.outStandingAmtLbl.textColor = UIColor.UCAColor.AppThemeTxtColor
        self.outStandingAmtTopView.backgroundColor = UIColor.UCAColor.AppThemeColor
        
        self.outStandingAmtValueLbl.text = self.userProfileJson.get("fOutStandingAmountWithSymbol")
        self.outStandingAmtValueLbl.textColor = UIColor.UCAColor.AppThemeTxtColor
        
        self.payNowLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PAY_NOW")
        self.adjustAmtLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADJUST_TRIP_TXT")
        
        GeneralFunctions.setImgTintColor(imgView: payNowImgView, color: UIColor(hex: 0xc4c4c4))
        GeneralFunctions.setImgTintColor(imgView: adjustAmtImgView, color: UIColor(hex: 0xc4c4c4))
        
        self.cancelOutAmtBtn.clickDelegate = self
        self.cancelOutAmtBtn.setButtonTitle(buttonTitle: generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TXT"))
        
        let adjustTripAmtGue = UITapGestureRecognizer()
        adjustTripAmtGue.addTarget(self, action: #selector(self.adjustOutStndAmt))
        
        self.adjustAmtView.isUserInteractionEnabled = true
        self.adjustAmtView.addGestureRecognizer(adjustTripAmtGue)
        
        let payNowGue = UITapGestureRecognizer()
        payNowGue.addTarget(self, action: #selector(self.payNowOutAmt))
        
        self.payNowView.isUserInteractionEnabled = true
        self.payNowView.addGestureRecognizer(payNowGue)
        
        if(self.userProfileJson.get("APP_PAYMENT_MODE") == "Cash"){
            self.payNowView.isHidden = true
            self.payNowViewHeight.constant = 0
        }
        
        UIView.animate(withDuration: 0.5,
                       animations: {
                        self.outStandingAmtBGView.alpha = 0.4
                        self.outStandingAmtView.alpha = 1
        },  completion: { finished in
            self.outStandingAmtBGView.alpha = 0.4
            self.outStandingAmtView.alpha = 1
        })
        
    }
    
    func payNowOutAmt(){
        closeOutStandingAmtView()
        self.checkCardMode = "OUT_STAND_AMT"
        isAutoContinue_payBox = false
        self.checkCardConfig()
    }
    
    func adjustOutStndAmt(){
        closeOutStandingAmtView()
        isAutoContinue_payBox = false
        isOutStandingAmtSkipped = true
        self.continueBtn.btnTapped()
    }
    
    func closeOutStandingAmtView(){
        if(self.outStandingAmtView != nil){
            self.outStandingAmtView.removeFromSuperview()
            self.outStandingAmtBGView.removeFromSuperview()
        }
    }
    
    @IBAction func unwindToUfxPayModeScreen(_ segue:UIStoryboardSegue) {
        //        unwindToSignUp
        self.userProfileJson = (GeneralFunctions.getValue(key: Utils.USER_PROFILE_DICT_KEY) as! String).getJsonDataDict().getObj(Utils.message_str)
        
        if(segue.source.isKind(of: AddPaymentUV.self))
        {
            self.selectCard()
        }
    }
}
