//
//  UFXChooseServicePhotoUV.swift
//  DriverApp
//
//  Created by ADMIN on 08/08/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit

class UFXChooseServicePhotoUV: UIViewController, MyLabelClickDelegate, MyBtnClickDelegate {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var pageHLbl: MyLabel!
    @IBOutlet weak var hintLbl: MyLabel!
    @IBOutlet weak var takePhotoImgView: UIImageView!
    @IBOutlet weak var continueBtn: MyButton!
    @IBOutlet weak var selectedImgBgView: UIView!
    @IBOutlet weak var selectedCheckImgView: UIImageView!
    @IBOutlet weak var skipLbl: MyLabel!
    
    
    var isTripEnd = false
    
    var activeTripUv:ActiveTripUV!
    
    let generalFunc = GeneralFunctions()
    
    
    let photoImgTapGue = UITapGestureRecognizer()
    let photoImgTapGueForPluseBtn = UITapGestureRecognizer()
    var openImgSelection:OpenImageSelectionOption!
    var isImageSelected = false
    
    var selectedFileData:Data!
    
    var address = ""
    var latitude = ""
    var longitude = ""
    var isPickUpMode = false
    var isTripCancelled = false
    var comment = ""
    var reason = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.contentView.addSubview(self.generalFunc.loadView(nibName: "UFXChooseServicePhotoScreenDesign", uv: self, contentView: contentView))
        
        self.addBackBarBtn()
        
       // self.selectedImgBgView.isHidden = true
       // self.selectedCheckImgView.isHidden = true
        
        //GeneralFunctions.setImgTintColor(imgView: selectedCheckImgView, color: UIColor.UCAColor.AppThemeColor)
        GeneralFunctions.setImgTintColor(imgView: selectedCheckImgView, color: UIColor.UCAColor.AppThemeTxtColor)
        self.selectedImgBgView.backgroundColor = UIColor.UCAColor.AppThemeColor
        
        setData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.activeTripUv.tripTaskExecuted = false
    }
    
    override func closeCurrentScreen() {
        self.activeTripUv.tripTaskExecuted = false
        super.closeCurrentScreen()
    }
    
    func setData(){
        self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_UPLOAD_IMAGE_SERVICE")
        self.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_UPLOAD_IMAGE_SERVICE")
        
        self.skipLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SKIP_TXT")
        
        if(isTripEnd){
            self.continueBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SAVE_PHOTO_END_SERVICE_TXT"))
            self.pageHLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_UPLOAD_SERVICE_AFTER_TXT")
        }else{
            self.continueBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SAVE_PHOTO_START_SERVICE_TXT"))
            self.pageHLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_UPLOAD_SERVICE_BEFORE_TXT")
        }
        self.hintLbl.text = self.generalFunc.getLanguageLabel(origValue: "Take Or Select Photo", key: "LBL_TAKE_PHOTO_OR_SELECT")
        self.pageHLbl.fitText()
        
        photoImgTapGue.addTarget(self, action: #selector(self.photoImgTapped))
        takePhotoImgView.isUserInteractionEnabled = true
        takePhotoImgView.addGestureRecognizer(photoImgTapGue)
        
        photoImgTapGueForPluseBtn.addTarget(self, action: #selector(self.photoImgTapped))
        selectedCheckImgView.isUserInteractionEnabled = true
        selectedCheckImgView.addGestureRecognizer(photoImgTapGueForPluseBtn)
        
        self.continueBtn.clickDelegate = self
        self.skipLbl.setClickDelegate(clickDelegate: self)
        self.hintLbl.setClickDelegate(clickDelegate: self)
    }
    
    func photoImgTapped(){
        
        self.openImgSelection = OpenImageSelectionOption(uv: self)
        self.openImgSelection.isUFXServicePhotoChoose = true
        self.openImgSelection.isChooseCategory = false
        self.openImgSelection.setImageSelectionHandler { (isImageSelected) in
            if(isImageSelected){
                self.isImageSelected = true
                self.selectedFileData = self.openImgSelection.selectedFileData
                self.selectedImgBgView.isHidden = false
                self.selectedCheckImgView.isHidden = false
                self.takePhotoImgView.image = UIImage(data: self.selectedFileData) //self.selectedImage//UIImage(named: "ic_cam")
                self.selectedCheckImgView.image = UIImage(named: "ic_edit")
                self.selectedImgBgView.isHidden = true
                self.selectedCheckImgView.isHidden = true
                
            }
        }
        self.openImgSelection.show { (isImageUpload) in
            if(isImageUpload == true){
                
                self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "Your document is uploaded successfully", key: "LBL_UPLOAD_DOC_SUCCESS"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                    
                    self.performSegue(withIdentifier: "unwindToActiveTrip", sender: self)
                    
                })
            }
        }
    }
    
    func myBtnTapped(sender: MyButton) {
        if(sender == self.continueBtn){
            if(self.isImageSelected == false){
                Utils.showSnakeBar(msg: self.generalFunc.getLanguageLabel(origValue: "Please select image", key: "LBL_SELECT_IMAGE_ERROR"), uv: self)
            }else{
                self.performSegue(withIdentifier: "unwindToActiveTrip", sender: self)
            }
        }
    }
    
    func myLableTapped(sender: MyLabel) {
        if(sender == self.hintLbl){
            photoImgTapped()
        }else if(sender == self.skipLbl){
            self.isImageSelected = false
            self.performSegue(withIdentifier: "unwindToActiveTrip", sender: self)
        }
    }
    
}
