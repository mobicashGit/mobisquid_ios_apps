//
//  UFXUpdateDayAvailabilityUV.swift
//  DriverApp
//
//  Created by ADMIN on 18/10/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit

class UFXUpdateDayAvailabilityUV: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, MyBtnClickDelegate {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var selectTimeSlotsBelowLbl: MyLabel!
    @IBOutlet weak var updateBtn: MyButton!
    
    var selectedDay = ""
    var dayTitle = ""
    var cntView:UIView!
    
    let generalFunc = GeneralFunctions()
    
    var timeArr = [String]()
    var timeAMPMArr = [String]()
    var selectedTimeArr = [Bool]()
    var time24Arr = [String]()
//    var selectedTime = ""
    
    var loaderView:UIView!
    
    var isDataSet = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addBackBarBtn()
        
        cntView = self.generalFunc.loadView(nibName: "UFXUpdateDayAvailabilityScreenDesign", uv: self, contentView: contentView)
        
        cntView.backgroundColor = UIColor.clear
        self.contentView.addSubview(cntView)
        
        cntView.isHidden = true
        
        setData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if(isDataSet == false){
            
            cntView.frame = self.contentView.frame
            
            cntView.isHidden = false
            
            retrieveAvailableTimeSlots()
            
            isDataSet = true
        }
        
    }

    func setData(){
        
        self.navigationItem.title = dayTitle
        self.title = dayTitle
        
        self.updateBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "UPDATE", key: "LBL_UPDATE_GENERAL"))
        self.updateBtn.clickDelegate = self
        
        self.selectTimeSlotsBelowLbl.text = self.generalFunc.getLanguageLabel(origValue: "Select time slots below", key: "LBL_SELECT_TIME_SLOT")
        
//        self.selectTimeSlotsBelowLbl.fitText()
        
        
        
        self.collectionView.register(UINib(nibName: "UFXUpdateDayAvailabilityCVCell", bundle: nil), forCellWithReuseIdentifier: "UFXUpdateDayAvailabilityCVCell")
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        let layout = KTCenterFlowLayout()
        layout.minimumInteritemSpacing = 10.0
        layout.minimumLineSpacing = 10.0
        layout.itemSize = CGSize(width: 105, height: 45)
        self.collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    func generateData(vAvailableTimesArr:[String]){
        self.selectedTimeArr.removeAll()
        self.timeAMPMArr.removeAll()
        self.timeArr.removeAll()
        
        for i in 0..<24 {
            
            var fromTime = i
            var toTime = i + 1
            
            if(fromTime < 12){
                self.timeAMPMArr += ["\(self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_AM_TXT"))"]
            }else{
                self.timeAMPMArr += ["\(self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PM_TXT"))"]
            }
            
            
            let tempChkTime = "\(fromTime < 10 ? "\(fromTime < 1 ? "12" : "0\(fromTime)")" : "\(fromTime)")-\(toTime < 10 ? "0\(toTime)" : "\(toTime)")"
            
            var isDataMatch = false
            
            for j in 0..<vAvailableTimesArr.count{
                let item = String(vAvailableTimesArr[j])
                if(tempChkTime == item){
                    isDataMatch = true
                    break
                }
            }
            
            selectedTimeArr += [isDataMatch]
            
            time24Arr += ["\(fromTime < 10 ? "0\(fromTime)" : "\(fromTime)")" + "-" + "\(toTime < 10 ? "0\(toTime)" : "\(toTime)")"]
            
            fromTime = fromTime % 12
            toTime = toTime % 12
            if(fromTime == 0){
                fromTime = 12
            }
            
            if(toTime == 0){
                toTime = 12
            }
            
            timeArr += ["\(Configurations.convertNumToAppLocal(numStr: "\(fromTime < 10 ? "0\(fromTime)" : "\(fromTime)")")) - \(Configurations.convertNumToAppLocal(numStr: "\(toTime < 10 ? "0\(toTime)" : "\(toTime)")"))"]
        }
        
        self.collectionView.reloadData()
    }
    
//    func getIndexOfSelectedTime() -> Int{
//        for i in 0..<self.timeArr.count{
//            if(self.selectedTime == self.timeArr[i]){
//                return i
//            }
//        }
//        return 0
//    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.timeArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UFXUpdateDayAvailabilityCVCell", for: indexPath) as! UFXUpdateDayAvailabilityCVCell
        cell.timeLbl.text = "\(self.timeArr[indexPath.item]) \(self.timeAMPMArr[indexPath.item])"
        
        if(self.selectedTimeArr[indexPath.item] == true){
            cell.timeLbl.textColor = UIColor.UCAColor.AppThemeTxtColor
            cell.timeLbl.backgroundColor = UIColor.UCAColor.AppThemeColor
        }else{
            cell.timeLbl.textColor = UIColor(hex: 0x1c1c1c)
            cell.timeLbl.backgroundColor = UIColor.clear
        }
        
        cell.timeLbl.layer.masksToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let previousItemIndex = self.getIndexOfSelectedTime()
//        self.selectedTime = self.timeArr[indexPath.item]
        if( self.selectedTimeArr[indexPath.item] == true){
            self.selectedTimeArr[indexPath.item] = false
        }else{
            self.selectedTimeArr[indexPath.item] = true
        }
        
        self.collectionView.reloadItems(at: [IndexPath(row: indexPath.item, section: indexPath.section)])
    }
    
    func retrieveAvailableTimeSlots(){
        self.cntView.isHidden = true
        
        if(loaderView == nil){
            loaderView =  self.generalFunc.addMDloader(contentView: self.contentView)
            loaderView.backgroundColor = UIColor.clear
        }else if(loaderView != nil){
            loaderView.isHidden = false
        }
        
        let parameters = ["type": "DisplayAvailability", "UserType": Utils.appUserType, "iDriverId": GeneralFunctions.getMemberd(), "vDay": self.selectedDay]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: false)
        
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    let vAvailableTimesArr = dataDict.getObj(Utils.message_str).get("vAvailableTimes").components(separatedBy: ",")
                    self.generateData(vAvailableTimesArr: vAvailableTimesArr)
                    
                    self.cntView.isHidden = false
                }else{
                    
                    if(dataDict.get(Utils.message_str) == "LBL_NO_AVAILABILITY_FOUND"){
                        self.generateData(vAvailableTimesArr: [])
                        
                        self.cntView.isHidden = false
                        
                    }else{
                        self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get(Utils.message_str)), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                            
                            //                        self.loadData()
                            self.closeCurrentScreen()
                        })
                    }
                    
                }
            }else{
                self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: InternetConnection.isConnectedToNetwork() ? "Please try again later" : "No Internet Connection", key: InternetConnection.isConnectedToNetwork() ? "LBL_TRY_AGAIN_TXT" : "LBL_NO_INTERNET_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                    
                    //                        self.loadData()
                    self.closeCurrentScreen()
                })
            }
            
            
            self.loaderView.isHidden = true
            
        })
    }

    func myBtnTapped(sender: MyButton) {
        if(sender == self.updateBtn){
            var vAvailableTimes = ""
            
            for i in 0..<selectedTimeArr.count{
                if(self.selectedTimeArr[i] == true){
                    vAvailableTimes =  vAvailableTimes == "" ? "\(self.time24Arr[i].trimAll())" : "\(vAvailableTimes),\(self.time24Arr[i].trimAll())"
                }
            }
            
            
            updateAvailability(vAvailableTimes: vAvailableTimes)
        }
    }
    
    func updateAvailability(vAvailableTimes:String){
        let parameters = ["type": "UpdateAvailability", "UserType": Utils.appUserType, "iDriverId": GeneralFunctions.getMemberd(), "vDay": self.selectedDay, "vAvailableTimes": vAvailableTimes]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get(Utils.message_str)), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                        
//                        self.closeCurrentScreen()
                        self.performSegue(withIdentifier: "unwindToSelectAvailDayScreen", sender: self)
                    })
                }else{
                    
                    self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get(Utils.message_str)), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                        
                    })
                }
            }else{
                self.generalFunc.setError(uv: self)
            }
            
        })
    }
}
