//
//  UFXSelectAvailabilityDayUV.swift
//  DriverApp
//
//  Created by ADMIN on 18/10/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit

class UFXSelectAvailabilityDayUV: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var selectDayBelowLbl: MyLabel!
    
    var cntView:UIView!
    
    let generalFunc = GeneralFunctions()
    
    var dataArr = [String]()
    var dataSetArr = [Bool]()
    
    var loaderView:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addBackBarBtn()
        
        cntView = self.generalFunc.loadView(nibName: "UFXSelectAvailabilityDayScreenDesign", uv: self, contentView: contentView)
        
        self.contentView.addSubview(cntView)
        
        setData()
        
        getDayData()
    }

    func setData() {
        self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "My Availability", key: "LBL_MY_AVAILABILITY")
        self.title = self.generalFunc.getLanguageLabel(origValue: "My Availability", key: "LBL_MY_AVAILABILITY")
        
        self.selectDayBelowLbl.text = self.generalFunc.getLanguageLabel(origValue: "Select Day Below", key: "LBL_SELECT_BELOW_TIMES_SLOT_TXT")
//        self.selectDayBelowLbl.fitText()
        
        let dt = DateFormatter()
        dt.locale = Locale(identifier: Configurations.getGoogleMapLngCode())
        dataArr = dt.shortWeekdaySymbols
        
        
        self.collectionView.register(UINib(nibName: "UFXSelectAvailabilityDayCVCell", bundle: nil), forCellWithReuseIdentifier: "UFXSelectAvailabilityDayCVCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.collectionView.reloadData()
    }
    
    func getDayData(){
        self.cntView.isHidden = true
        
        if(loaderView == nil){
            loaderView =  self.generalFunc.addMDloader(contentView: self.view)
        }else{
            loaderView.isHidden = false
        }
        
        let parameters = ["type":"DisplayDriverDaysAvailability", "iDriverId": GeneralFunctions.getMemberd(), "UserType": CommonUtils.app_user_name]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: false)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                
                if(dataDict.get("Action") == "1"){
                    
                    let msgArr = dataDict.getArrObj(Utils.message_str)
                    
                    var weekDaysArr = self.getEngWeekDayArr()
                    self.dataSetArr.removeAll()
                    
                    if(weekDaysArr != nil){
                        for i in 0..<weekDaysArr!.count{
                            let weekDay_str = weekDaysArr![i]
                            var isDayDataAdded = false
                            for j in 0..<msgArr.count{
                                let msgTemp = (msgArr[j] as! NSDictionary).get("vDay")
                                if(weekDay_str.uppercased() == msgTemp.uppercased()){
                                    isDayDataAdded = true
                                    break
                                }
                            }
                            
                            self.dataSetArr += [isDayDataAdded]
                        }
                    }
                }else{
                    
                    if(dataDict.get(Utils.message_str) == "LBL_NO_AVAILABILITY_FOUND"){
                        
                        self.cntView.isHidden = false
                        
                    }else{
                        self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get(Utils.message_str)), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                            
                            //                        self.loadData()
                            self.closeCurrentScreen()
                        })
                    }
                    
                }
                
                
                
            }else{
                self.generalFunc.setError(uv: self)
            }
            
            self.collectionView.reloadData()
            self.cntView.isHidden = false
            self.loaderView.isHidden = true
        })
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return dataArr.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {


        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UFXSelectAvailabilityDayCVCell", for: indexPath) as! UFXSelectAvailabilityDayCVCell

        cell.dayLbl.text = dataArr[indexPath.item]
        
        if(self.dataSetArr.count > indexPath.item && self.dataSetArr[indexPath.item] == true){
            cell.dayLbl.textColor = UIColor.UCAColor.AppThemeTxtColor
            cell.dayLbl.backgroundColor = UIColor.UCAColor.AppThemeColor
        }else{
            cell.dayLbl.textColor = UIColor(hex: 0x1c1c1c)
            cell.dayLbl.backgroundColor = UIColor(hex: 0xCCD6DD)
        }

        Utils.createRoundedView(view: cell.dayLbl, borderColor: UIColor.clear, borderWidth: 0)

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var weekDaysArr = getEngWeekDayArr()
        let ufxUpdateDayAvailabilityUV = GeneralFunctions.instantiateViewController(pageName: "UFXUpdateDayAvailabilityUV") as! UFXUpdateDayAvailabilityUV
        ufxUpdateDayAvailabilityUV.selectedDay = (weekDaysArr != nil && weekDaysArr!.count > indexPath.item) ? weekDaysArr![indexPath.item] : dataArr[indexPath.item]
        ufxUpdateDayAvailabilityUV.dayTitle = dataArr[indexPath.item]
        self.pushToNavController(uv: ufxUpdateDayAvailabilityUV)
    }
    
    func getEngWeekDayArr() -> [String]?{
        
        let dt = DateFormatter()
        dt.locale = Locale(identifier: "en")
        return dt.weekdaySymbols
    }
    
    @IBAction func unwindToSelectAvailDayScreen(_ segue:UIStoryboardSegue) {
        
        if(segue.source .isKind(of: UFXUpdateDayAvailabilityUV.self)){
            self.getDayData()
        }
    }
}
