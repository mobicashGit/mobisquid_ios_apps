//
//  OpenEnterUFXAvailRadiusView.swift
//  DriverApp
//
//  Created by ADMIN on 18/10/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit

class OpenEnterUFXAvailRadiusView: NSObject, MyLabelClickDelegate {
    
    typealias CompletionHandler = (_ isRadiusChanged:Bool, _ radius:String) -> Void
    
    var uv:UIViewController!
    var containerView:UIView!
    
    let generalFunc = GeneralFunctions()
    var enterUFXAvailabilityRadiusView:EnterUFXAvailabilityRadiusView!
    var bgView:UIView!
    var handler:CompletionHandler!
    var userProfileJson:NSDictionary!
    var loadingDialog:NBMaterialLoadingDialog!
    
    init(uv:UIViewController, containerView:UIView, userProfileJson: NSDictionary){
        self.uv = uv
        self.containerView = containerView
        self.userProfileJson = userProfileJson
        super.init()
    }
    
    func setViewHandler(handler: @escaping CompletionHandler){
        self.handler = handler
    }
    
    func show(){
        bgView = UIView()
        bgView.backgroundColor = UIColor.black
        bgView.alpha = 0.4
        //        bgView.frame = self.containerView.frame
        bgView.frame = CGRect(x:0, y:0, width:Application.screenSize.width, height: Application.screenSize.height)
        
        bgView.center = CGPoint(x: Application.screenSize.width / 2, y: Application.screenSize.height / 2)
        
        let bgTapGue = UITapGestureRecognizer()
        bgTapGue.addTarget(self, action: #selector(self.removeView))
        bgView.addGestureRecognizer(bgTapGue)
        
        let viewWidth = (Application.screenSize.width > 390 ? 375 : (Application.screenSize.width - 50))
        
        let subHstr = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_WORK_RADIUS_HEADER")
        let extraHeight = subHstr.height(withConstrainedWidth: viewWidth - 40, font: UIFont(name: "Roboto-Light", size: 17)!) - 20
        
        
        enterUFXAvailabilityRadiusView = EnterUFXAvailabilityRadiusView(frame: CGRect(x: Application.screenSize.width / 2, y: Application.screenSize.height / 2, width: viewWidth, height: 202 + extraHeight))
        
        enterUFXAvailabilityRadiusView.center = CGPoint(x: Application.screenSize.width / 2, y: Application.screenSize.height / 2)
        
        enterUFXAvailabilityRadiusView.radiusHLbl.text = self.generalFunc.getLanguageLabel(origValue: "Radius", key: "LBL_RADIUS")
        
        enterUFXAvailabilityRadiusView.radiusSubLbl.text = subHstr
        enterUFXAvailabilityRadiusView.radiusSubLbl.fitText()
        
        enterUFXAvailabilityRadiusView.radiusTxtField.maxCharacterLimit = 4
        enterUFXAvailabilityRadiusView.radiusTxtField.setText(text: userProfileJson.get("vWorkLocationRadius"))
        enterUFXAvailabilityRadiusView.radiusTxtField.getTextField()!.placeholderAnimation = .hidden
        enterUFXAvailabilityRadiusView.radiusTxtField.getTextField()!.keyboardType = .numberPad
        
        enterUFXAvailabilityRadiusView.positiveLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_OK_TXT")
        enterUFXAvailabilityRadiusView.negativeLbl.text = self.generalFunc.getLanguageLabel(origValue: "Cancel", key: "LBL_CANCEL_TXT")
        
        enterUFXAvailabilityRadiusView.positiveLbl.setClickDelegate(clickDelegate: self)
        enterUFXAvailabilityRadiusView.negativeLbl.setClickDelegate(clickDelegate: self)
        
        Utils.createRoundedView(view: enterUFXAvailabilityRadiusView, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 10)
        
        enterUFXAvailabilityRadiusView.layer.shadowOpacity = 0.5
        enterUFXAvailabilityRadiusView.layer.shadowOffset = CGSize(width: 0, height: 3)
        enterUFXAvailabilityRadiusView.layer.shadowColor = UIColor.black.cgColor
        
        
//        let currentWindow = Application.window
//
//        if(currentWindow != nil){
//            currentWindow?.addSubview(bgView)
//            currentWindow?.addSubview(enterUFXAvailabilityRadiusView)
//        }else{
//            self.uv.view.addSubview(bgView)
//            self.uv.view.addSubview(enterUFXAvailabilityRadiusView)
//        }
        
        let currentWindow = Application.window
        
        if(self.uv == nil){
            currentWindow?.addSubview(bgView)
            currentWindow?.addSubview(enterUFXAvailabilityRadiusView)
        }else if(self.uv.navigationController != nil){
            self.uv.navigationController?.view.addSubview(bgView)
            self.uv.navigationController?.view.addSubview(enterUFXAvailabilityRadiusView)
            
            enterUFXAvailabilityRadiusView.tag = Utils.ALERT_DIALOG_CONTENT_TAG
            bgView.tag = Utils.ALERT_DIALOG_BG_TAG
        }else{
            self.uv.view.addSubview(bgView)
            self.uv.view.addSubview(enterUFXAvailabilityRadiusView)
        }
    }
    
    func myLableTapped(sender: MyLabel) {
        if(sender == self.enterUFXAvailabilityRadiusView.positiveLbl){
            checkData()
        }else if(sender == self.enterUFXAvailabilityRadiusView.negativeLbl){
            removeView()
        }
    }
    func removeView(){
        enterUFXAvailabilityRadiusView.frame.origin.y = Application.screenSize.height + 2500
        enterUFXAvailabilityRadiusView.removeFromSuperview()
        
        UIApplication.shared.isStatusBarHidden = true
        UIApplication.shared.isStatusBarHidden = false
        
        bgView.removeFromSuperview()
        
        self.uv.view.layoutIfNeeded()
    }
    
    func checkData(){
        let required_str = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_FEILD_REQUIRD_ERROR_TXT")
       
        let isRadiusEntered = Utils.checkText(textField: enterUFXAvailabilityRadiusView.radiusTxtField.getTextField()!) ? true : Utils.setErrorFields(textField: self.enterUFXAvailabilityRadiusView.radiusTxtField.getTextField()!, error: required_str)
        
        if(isRadiusEntered  == true){
            DispatchQueue.main.async() {
                self.changeRadius(radius: Utils.getText(textField: self.enterUFXAvailabilityRadiusView.radiusTxtField.getTextField()!))
            }
        }
        
    }
    
    func changeRadius(radius:String){
        
        self.loadingDialog = NBMaterialLoadingDialog.showLoadingDialogWithText(self.enterUFXAvailabilityRadiusView, isCancelable: false, message: (GeneralFunctions()).getLanguageLabel(origValue: "Loading", key: "LBL_LOADING_TXT"))
        
        let parameters = ["type":"UpdateRadius","iDriverId": GeneralFunctions.getMemberd(), "vWorkLocationRadius": radius, "UserType": Utils.appUserType]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.uv.view, isOpenLoader: false)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: true)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(self.loadingDialog != nil){
                self.loadingDialog.hideDialog()
            }
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                     GeneralFunctions.saveValue(key: Utils.USER_PROFILE_DICT_KEY, value: response as AnyObject)
                    self.removeView()
                    
                    if(self.handler != nil){
                        self.handler(true, radius)
                    }
                    
                }else{
                    self.generalFunc.setError(uv: self.uv, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.generalFunc.setError(uv: self.uv)
            }
        })
        
        
    }
    
}

