//
//  EnterUFXAvailabilityRadiusView.swift
//  DriverApp
//
//  Created by ADMIN on 18/10/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit

class EnterUFXAvailabilityRadiusView: UIView {

    @IBOutlet weak var radiusHLbl: MyLabel!
    @IBOutlet weak var radiusTxtField: MyTextField!
    @IBOutlet weak var positiveLbl: MyLabel!
    @IBOutlet weak var negativeLbl: MyLabel!
    @IBOutlet weak var radiusSubLbl: MyLabel!

    
    var view: UIView!
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        view.frame = bounds
        
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "EnterUFXAvailabilityRadiusView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
}
